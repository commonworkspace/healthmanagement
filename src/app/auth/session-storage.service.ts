import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

const TOKEN_KEY = 'AuthToken';
const NICKNAME_KEY = 'Nickname';
const BRANCHNAME_KEY = 'BranchName';
const MCOMPANY_KEY = 'CompanyName';
@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  constructor(private injector: Injector, private router: Router) {}
  signOut() {
    window.sessionStorage.clear();
    const routerService = this.injector.get(Router);
    routerService.navigateByUrl('/', { skipLocationChange: false });
  }
  public saveToken(
    token: string,
    nickname: string,
    branchName: string,
    companyName: string
  ) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
    window.sessionStorage.setItem(NICKNAME_KEY, nickname);
    window.sessionStorage.setItem(BRANCHNAME_KEY, branchName);
    window.sessionStorage.setItem(MCOMPANY_KEY, companyName);
  }
  public tokenValidation() {
    const authKey = window.sessionStorage.getItem(TOKEN_KEY);
    console.log(authKey);
    if (authKey === null || authKey === undefined) {
      const routerService = this.injector.get(Router);
      routerService.navigateByUrl('/', { skipLocationChange: false });
      return true;
    } else {
      return false;
    }
    // window.sessionStorage.setItem(TOKEN_KEY, token);
  }
}

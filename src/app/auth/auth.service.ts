import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthLoginInfo } from './login-info';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Access-Control-Allow-Origin': '*',
//     'Content-Type': 'application/json',
//   }),
// };
// http://210.18.134.138:82/axpertwebx6scripts/
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private loginUrl =
    'http://210.18.134.138:82/axpertwebx6scripts/ASBMenuRest.dll/datasnap/rest/TASBMenuREST/Login';
  constructor(private http: HttpClient) {}
  attemptAuth(credentials: any): Observable<any> {
    return this.http.post<any>(this.loginUrl, credentials);
  }
}

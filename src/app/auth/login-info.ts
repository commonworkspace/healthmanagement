export class AuthLoginInfo {
  axpapp: string;
  username: string;
  password: Int32Array | string;
  seed: string;
  other: string;

  constructor(
    axpapp: string,
    username: string,
    password: Int32Array | string,
    seed: string,
    other: string
  ) {
    this.axpapp = axpapp;
    this.username = username;
    this.password = password;
    this.seed = seed;
    this.other = other;
  }
}

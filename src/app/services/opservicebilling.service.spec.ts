import { TestBed } from '@angular/core/testing';

import { OpservicebillingService } from './opservicebilling.service';

describe('OpservicebillingService', () => {
  let service: OpservicebillingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpservicebillingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

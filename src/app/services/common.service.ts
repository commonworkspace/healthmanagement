import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  getiviewApi = environment.getiviewApi;
  saveUrlApi = environment.saveUrlApi;
  constructor(private http: HttpClient) {}
  getMasterDataAPI(): Observable<any> {
    const masterData = {
      _parameters: [
        {
          getiview: {
            name: 'mstapiff',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {},
          },
        },
      ],
    };
    const result = JSON.stringify(masterData);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getCountryDataAPI(): Observable<any> {
    const country = {
      _parameters: [
        {
          getiview: {
            name: 'Country',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'false',
            params: {
              countryname: 'ALL',
            },
          },
        },
      ],
    };
    const result = JSON.stringify(country);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getStateDataAPI(country: string): Observable<any> {
    const stateApi = {
      _parameters: [
        {
          getiview: {
            name: 'State',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'false',
            params: {
              pcountry: country,
              pstate: 'ALL',
            },
          },
        },
      ],
    };
    const result = JSON.stringify(stateApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getDistrictDataAPI(state: string, country: string): Observable<any> {
    const districtApi = {
      _parameters: [
        {
          getiview: {
            name: 'City',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'false',
            params: {
              pcountryname: country,
              pstate: state,
              pcityname: 'ALL',
              act: 'Active',
            },
          },
        },
      ],
    };
    const result = JSON.stringify(districtApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getAreaDataAPI(
    country: string,
    state: string,
    city: string
  ): Observable<any> {
    const pincodeApi = {
      _parameters: [
        {
          getiview: {
            name: 'Pincode',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'false',
            params: {
              pcountryname: country,
              pstatename: state,
              pcityname: city,
              pincode: 'ALL',
            },
          },
        },
      ],
    };
    const result = JSON.stringify(pincodeApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  // getPincodeDataAPI(
  //   pincode: string,
  //   country: string,
  //   state: string,
  //   district: string
  // ): Observable<any> {
  //   const pincodeApi = {
  //     _parameters: [
  //       {
  //         getiview: {
  //           name: 'Pincode',
  //           axpapp: 'hmsbalaji',
  //           s: window.sessionStorage.getItem('AuthToken'),
  //           pageno: '1',
  //           pagesize: '1250',
  //           sqlpagination: 'true',
  //           params: {
  //             pcountryname: 'ALL',
  //             pstatename: 'ALL',
  //             pcityname: 'ALL',
  //             pincode: pincode,
  //           },
  //         },
  //       },
  //     ],
  //   };
  //   const result = JSON.stringify(pincodeApi);
  //   console.log(result);
  //   return this.http.post<any>(this.getiviewApi, result);
  // }
  getPriceListDataAPI(corporateid: string): Observable<any> {
    const pricelistApi = {
      _parameters: [
        {
          getiview: {
            name: 'apiprils',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              corporateid: corporateid,
              campid: '0',
              camp_name: '',
            },
          },
        },
      ],
    };
    const result = JSON.stringify(pricelistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getCorporateListDataAPI(): Observable<any> {
    const corporatelistApi = {
      _parameters: [
        {
          getiview: {
            name: 'apicorpr',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {},
          },
        },
      ],
    };
    const result = JSON.stringify(corporatelistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getConsultationDataAPI(
    companyid: string,
    branchid: string,
    doctor_viewid: string,
    pricelisthdrid: string,
    screentype: string
  ): Observable<any> {
    const consultationlistApi = {
      _parameters: [
        {
          getiview: {
            name: 'apicnsty',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              companyid: companyid,
              branchid: branchid,
              pricelisthdrid: pricelisthdrid,
              attendingdrid: doctor_viewid,
              screentype: screentype,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(consultationlistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getPackageDataAPI(
    companyid: string,
    branchid: string,
    pricelisthdrid: string
    // doctor_viewid: string
  ): Observable<any> {
    const packagelistApi = {
      _parameters: [
        {
          getiview: {
            name: 'packrate',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              companyid: companyid,
              branchid: branchid,
              pricelisthdrid: pricelisthdrid,
              // doctor_viewid: doctor_viewid,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(packagelistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getDocterMasterDataAPI(): Observable<any> {
    const docterMasterlistApi = {
      _parameters: [
        {
          getiview: {
            name: 'apidctr',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {},
          },
        },
      ],
    };
    const result = JSON.stringify(docterMasterlistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getSearchDataAPI(
    branch: string,
    uhid: string,
    pname: string,
    spousename: string,
    fname: string,
    gender: string,
    mobile: string,
    email: string,
    oldNo: string
  ): Observable<any> {
    const searchlistApi = {
      _parameters: [
        {
          getiview: {
            name: 'patsearc',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              company: window.sessionStorage.getItem('CompanyName'),
              branch: branch,
              mrno: oldNo,
              uuuid: uhid,
              patientname: pname,
              spousename: spousename,
              gender: gender,
              mobileprimary: mobile,
              email: email,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(searchlistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  getPatientListSearchDataAPI(patientType: string): Observable<any> {
    const searchPatientlistApi = {
      _parameters: [
        {
          getiview: {
            name: 'apioplst',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              type: patientType,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(searchPatientlistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  saveDataAPI(
    company,
    companyid,
    branchid,
    category,
    source,
    referred_by_physician,
    gender,
    slt,
    pname,
    fathername,
    dob,
    age,
    ageType,
    country,
    state,
    city,
    area,
    address,
    mobileNumber,
    corporateList,
    priceList,
    pcompany,
    physician,
    consultationType,
    consultationid,
    paymentMode,
    concessionvalue,
    eft,
    cardtype,
    cardName,
    cardNo,
    cardExpiry,
    ddNo,
    ddDate,
    ddBank
  ): Observable<any> {
    const saveDataApi = {
      _parameters: [
        {
          savedata: {
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            transid: 'oprgi',
            xmltext: '',
            recordid: '0',

            recdata: [
              {
                axp_recid1: [
                  {
                    rowno: '001',
                    text: '0',
                    columns: {
                      apicalledfrom: 'oprgi',
                      company: company,
                      companyid: companyid,
                      branch: window.sessionStorage.getItem('BranchName'),
                      branchid: branchid,
                      patientcategory: category,
                      source: source,
                      referred_by_physician: referred_by_physician,
                      gender: gender,
                      salutation_patient_name: slt,
                      patient_name: pname,
                      spouse_father_name: fathername,
                      date_of_birth: dob,
                      age: age,
                      agetype: ageType,
                      country: country,
                      state: state,
                      city: city,
                      area: area,
                      street_address: address,
                      mobile_primary: mobileNumber,
                      company_name: corporateList,
                      pricelist: priceList,
                      partner_company: pcompany,
                      docdept: physician,
                      consulttype: consultationType,
                      payment_mode: paymentMode,
                      concessionvalue: concessionvalue,
                      transaction_no: eft,
                      card_type: cardtype,
                      card_name: cardName,
                      card_no: cardNo,
                      expiry_date: cardExpiry,
                      chequeno: ddNo,
                      chequedate: ddDate,
                      chequebank: ddBank,
                    },
                  },
                ],
              },
            ],
          },
        },
      ],
    };

    const result = JSON.stringify(saveDataApi);
    console.log(result);
    return this.http.post<any>(this.saveUrlApi, result);
  }

  getPDFURL(id: string): Observable<any> {
    const pdfId = {
      _parameters: [
        {
          getiview: {
            name: 'gtpdfurl',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              pdocid: id,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(pdfId);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  saveNewMHCDataAPI(
    company,
    companyid,
    branchid,
    category,
    source,
    referred_by_physician,
    gender,
    slt,
    pname,
    fathername,
    dob,
    age,
    ageType,
    country,
    state,
    city,
    area,
    address,
    mobileNumber,
    company_name,
    priceList,
    partner_company,
    mhc_package,
    // mhc_packageid,
    physician,
    paymentMode,
    concessionvalue,
    eft,
    cardtype,
    cardName,
    cardNo,
    cardExpiry,
    ddNo,
    ddDate,
    ddBank
  ): Observable<any> {
    const saveDataApi = {
      _parameters: [
        {
          savedata: {
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            transid: 'mhcap',
            xmltext: '',
            recordid: '0',
            recdata: [
              {
                axp_recid1: [
                  {
                    rowno: '001',
                    text: '0',
                    columns: {
                      apicalledfrom: 'mhcap',
                      company: company,
                      companyid: companyid,
                      branch: window.sessionStorage.getItem('BranchName'),
                      branchid: branchid,
                      patientcategory: category,
                      source: source,
                      referred_by_physician: referred_by_physician,
                      gender: gender,
                      salutation_patient_name: slt,
                      patient_name: pname,
                      spouse_father_name: fathername,
                      date_of_birth: dob,
                      age: age,
                      agetype: ageType,
                      country: country,
                      state: state,
                      city: city,
                      area: area,
                      street_address: address,
                      mobile_primary: mobileNumber.toString(),
                      company_name: company_name,
                      pricelist: priceList,
                      partner_company: partner_company,
                      // docdept: physician,
                      mhc_package: mhc_package,
                      // mhc_package: {
                      //   text: mhc_package,
                      //   id: mhc_packageid,
                      //   oldid: '',
                      //   ov: '',
                      // },
                      attending_physician: physician,
                      // consulttype: consultationType,
                      payment_mode: paymentMode,
                      concessionvalue: concessionvalue,
                      transaction_no: eft,
                      card_type: cardtype,
                      card_name: cardName,
                      card_no: cardNo,
                      expiry_date: cardExpiry,
                      chequeno: ddNo,
                      chequedate: ddDate,
                      chequebank: ddBank,
                      FinYr: '2020',
                    },
                  },
                ],
              },
            ],
          },
        },
      ],
    };

    const result = JSON.stringify(saveDataApi);
    console.log(result);
    return this.http.post<any>(this.saveUrlApi, result);
  }
  saveMHCDataAPI(
    source,
    referred_by_physician,
    uhid,
    pname,
    mobileNo,
    dob,
    mhc_package,
    physician,
    company_name,
    priceList,
    partner_company,
    package_fee,
    concession,
    paymentMode,
    concessionvalue,
    payment_amount,
    eft,
    cardtype,
    cardName,
    cardNo,
    cardExpiry,
    ddNo,
    ddDate,
    ddBank
  ): Observable<any> {
    const saveDataApi = {
      _parameters: [
        {
          savedata: {
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            transid: 'remha',
            xmltext: '',
            recordid: '0',

            recdata: [
              {
                axp_recid1: [
                  {
                    rowno: '001',
                    text: '0',
                    columns: {
                      apicalledfrom: 'remha',
                      company: window.sessionStorage.getItem('CompanyName'),
                      branch: window.sessionStorage.getItem('BranchName'),
                      source: source,
                      referred_by_physician: referred_by_physician,
                      uhid: uhid,
                      patient_name: pname,
                      mobile_primary: mobileNo.toString(),
                      date_of_birth: dob,
                      mhc_package: mhc_package,
                      attending_physician: physician,
                      company_name: company_name,
                      pricelist: priceList,
                      partner_company: partner_company,
                      package_fee: package_fee,
                      concession: concession.toString(),
                      payment_mode: paymentMode,
                      concessionvalue: concessionvalue,
                      payment_amount: payment_amount.toString(),
                      transaction_no: eft,
                      card_type: cardtype,
                      card_name: cardName,
                      card_no: cardNo,
                      expiry_date: cardExpiry,
                      chequeno: ddNo,
                      chequedate: ddDate,
                      chequebank: ddBank,
                      FinYr: '2020',
                    },
                  },
                ],
              },
            ],
          },
        },
      ],
    };

    const result = JSON.stringify(saveDataApi);
    console.log(result);
    return this.http.post<any>(this.saveUrlApi, result);
  }
  saveVisitEntryDataAPI(
    uhid,
    mobileNo,
    dob,
    source,
    referred_by_physician,
    dept_op,
    company_name,
    priceList,
    partner_company,
    consultationType,
    concession,
    paymentMode,
    concessionvalue,
    payment_amount,
    eft,
    cardtype,
    cardName,
    cardNo,
    cardExpiry,
    ddNo,
    ddDate,
    ddBank
  ): Observable<any> {
    const saveDataApi = {
      _parameters: [
        {
          savedata: {
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            transid: 'revap',
            xmltext: '',
            recordid: '0',

            recdata: [
              {
                axp_recid1: [
                  {
                    rowno: '001',
                    text: '0',
                    columns: {
                      apicalledfrom: 'revap',
                      company: window.sessionStorage.getItem('CompanyName'),
                      branch: window.sessionStorage.getItem('BranchName'),
                      uhid: uhid,
                      mobile_primary: mobileNo,
                      date_of_birth: dob,
                      source: source,
                      referred_by_physician: referred_by_physician,
                      dept_op: dept_op,
                      company_name: company_name,
                      pricelist: priceList,
                      partner_company: partner_company,
                      consultationtype: consultationType,
                      concession: concession.toString(),
                      payment_mode: paymentMode,
                      concessionvalue: concessionvalue,
                      payment_amount: payment_amount.toString(),
                      transaction_no: eft,
                      card_type: cardtype,
                      card_name: cardName,
                      card_no: cardNo,
                      expiry_date: cardExpiry,
                      chequeno: ddNo,
                      chequedate: ddDate,
                      chequebank: ddBank,
                    },
                  },
                ],
              },
            ],
          },
        },
      ],
    };

    const result = JSON.stringify(saveDataApi);
    console.log(result);
    return this.http.post<any>(this.saveUrlApi, result);
  }

  getOpServiceDataAPI(branchid: string): Observable<any> {
    const opServiceDatalistApi = {
      _parameters: [
        {
          getiview: {
            name: 'opservc',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              branchid: branchid,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(opServiceDatalistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }

  getOpServiceRateDataAPI(
    branchid,
    pricelisthdrid,
    serviceid,
    doctorid,
    companyid
  ): Observable<any> {
    const OpServiceRateApi = {
      _parameters: [
        {
          getiview: {
            name: 'oprateap',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {
              branchid: branchid,
              pricelisthdrid: pricelisthdrid,
              serviceid: serviceid,
              doctorid: doctorid,
              companyid: companyid,
            },
          },
        },
      ],
    };
    const result = JSON.stringify(OpServiceRateApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
  billingValueObject(
    billingScreen,
    billtype,
    paymentMode,
    concessionSource,
    concessionReferredBy,
    authorized,
    topayby,
    ostransfer,
    remark,
    eft,
    cardtype,
    cardName,
    cardNo,
    ddNo,
    ddDate,
    ddBank
  ): Observable<any> {
    const OpServiceRateApi = {
      _parameters: [
        {
          savedata: {
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            transid: 'obill',
            xmltext: '',
            recordid: '0',

            recdata: [
              {
                axp_recid1: [
                  {
                    rowno: '001',
                    text: '0',
                    columns: {
                      apicalledfrom: 'obill',
                      company: window.sessionStorage.getItem('CompanyName'),
                      branch: window.sessionStorage.getItem('BranchName'),
                      uhid: history.state.data.uhid,
                      opip_no: history.state.data.opn,
                      billtype: billtype,
                      modeofpayment: paymentMode,
                      concession_source: concessionSource,
                      concessionby: concessionReferredBy,
                      authorizedby: authorized,
                      tobepay: topayby,
                      ostransfer: ostransfer,
                      seft_transid: eft,
                      scard_no: cardNo,
                      scard_name: cardName,
                      scard_type: cardtype,
                      scheque_dd_no: ddNo,
                      scheque_dd_date: ddDate,
                      scheque_dd_bank: ddBank,
                      Remarks: remark,
                      FinYr: '2020',
                    },
                  },
                ],
              },
              {
                axp_recid2: billingScreen,
              },
            ],
          },
        },
      ],
    };
    const result = JSON.stringify(OpServiceRateApi);
    console.log(result, 'OpServiceRateApi');
    return this.http.post<any>(this.saveUrlApi, result);
    // const billingScreen
    // console.log(billingScreen);
    // return;
  }
  getReferredDocterDataAPI(): Observable<any> {
    const getReferredDocterlistApi = {
      _parameters: [
        {
          getiview: {
            name: 'apcnsapr',
            axpapp: 'hmsbalaji',
            s: window.sessionStorage.getItem('AuthToken'),
            pageno: '1',
            pagesize: '1250',
            sqlpagination: 'true',
            params: {},
          },
        },
      ],
    };
    const result = JSON.stringify(getReferredDocterlistApi);
    console.log(result);
    return this.http.post<any>(this.getiviewApi, result);
  }
}

import { Injectable } from '@angular/core';
import { OpBillingServices } from 'app/models/op-billing-services';

@Injectable({
  providedIn: 'root',
})
export class OpservicebillingService {
  opBillingServiceList: OpBillingServices[] = [];

  constructor() {}

  addService(service: OpBillingServices) {
    service.sno = this.opBillingServiceList.length + 1;
    this.opBillingServiceList.push(service);
  }

  editService(service: OpBillingServices) {
    const index = this.opBillingServiceList.findIndex(
      (c) => c.sno === service.sno
    );
    this.opBillingServiceList[index] = service;
  }

  deleteService(sno: number) {
    const service = this.opBillingServiceList.findIndex((c) => c.sno === sno);
    this.opBillingServiceList.splice(service, 1);
  }

  getAllService() {
    return this.opBillingServiceList;
  }
}

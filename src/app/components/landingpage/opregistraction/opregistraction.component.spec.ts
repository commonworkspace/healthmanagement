import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpregistractionComponent } from './opregistraction.component';

describe('OpregistractionComponent', () => {
  let component: OpregistractionComponent;
  let fixture: ComponentFixture<OpregistractionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpregistractionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpregistractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

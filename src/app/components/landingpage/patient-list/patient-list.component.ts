import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { SessionStorageService } from 'app/auth/session-storage.service';
import { CommonService } from 'app/services/common.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css'],
})
export class PatientListComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  public searchList: any = [];
  search: FormGroup;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  dataSource: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  displayedColumns: string[] = [
    'UHID',
    'OP No',
    'Patient Name',
    'Doctor Name',
    'Mobile No',
    'States',
    'Service Billing',
  ];

  constructor(
    private fb: FormBuilder,
    private sessionService: SessionStorageService,
    private commonService: CommonService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sessionService.tokenValidation();
    this.search = this.fb.group({
      patientType: ['All'],
    });
    this.searchCall();
  }

  public async searchCall() {
    const patientType = this.search.get('patientType').value;
    await this.commonService.getPatientListSearchDataAPI(patientType).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        // console.log(data);
        this.accordion.closeAll();
        this.searchList = data.result[0].row;
        this.dataSource = new MatTableDataSource<[]>(this.searchList);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };

  navServiceBilling(element) {
    // const navigationExtras: NavigationExtras = {
    //   state: { propertyType: propertyType, propertyCode: propertyCode },
    // };
    this.router.navigate(['home/opservicebilling'], {
      state: { data: element },
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhcregistrationComponent } from './mhcregistration.component';

describe('MhcregistrationComponent', () => {
  let component: MhcregistrationComponent;
  let fixture: ComponentFixture<MhcregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhcregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhcregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

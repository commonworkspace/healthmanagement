import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { SessionStorageService } from 'app/auth/session-storage.service';
import { CommonService } from 'app/services/common.service';
import { MatStepper } from '@angular/material/stepper';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { MatDatepicker } from '@angular/material/datepicker';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-mhcregistration',
  templateUrl: './mhcregistration.component.html',
  styleUrls: ['./mhcregistration.component.css'],
})
export class MhcregistrationComponent implements OnInit {
  @ViewChild('stepper') private myStepper: MatStepper;
  @ViewChild('picker') picker2: MatDatepicker<Date>;
  @ViewChild('picker') picker3: MatDatepicker<Date>;
  general: FormGroup;
  corporate: FormGroup;
  payment: FormGroup;
  public sourceTypes: any = [];
  public refDocterTypes: any = [];
  public refByDocter: boolean = false;
  public paymentModeTypes: any = [];
  public attendingPhysicianTypes: any = [];
  public packageTypes: any = [];
  public corporateListTypes: any = [];
  public priceListTypes: any = [];
  public bankTypes: any = [];
  public expiredMinDate = new Date();
  public expiredMaxDate = new Date(2500, 1, 1);
  public ddexpiredMinDate = new Date();
  public ddexpiredMaxDate = new Date(2500, 1, 1);

  filteredPhysician: Observable<any[]>;
  filteredCorporate: Observable<any[]>;
  filteredReferredBy: Observable<any[]>;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  printBtn: boolean = false;
  saveBth: boolean = true;
  saveAndPrintBtn: boolean = true;
  public printId = '';

  constructor(
    private injector: Injector,
    private router: Router,
    private sessionService: SessionStorageService,
    private fb: FormBuilder,
    private commonService: CommonService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.sessionService.tokenValidation();
    if (history.state.data === undefined) {
      const routerService = this.injector.get(Router);
      routerService.navigateByUrl('/home/patientsearch', {
        skipLocationChange: false,
      });
    } else {
      // console.log(history.state.data, 'mhc');
      this.general = this.fb.group({
        source: ['Walk In', Validators.required],
        referredBy: [''],
        uhid: [''],
        opno: ['Auto'],
        pname: ['', Validators.required],
        age: ['', Validators.required],
        ageType: ['Years', Validators.required],
        mobileNo: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('[6-9]\\d{9}'),
          ]),
        ],
      });
      this.corporate = this.fb.group({
        corporateList: [''],
        priceList: [''],
        physician: ['', Validators.required],
        package: ['', Validators.required],
      });
      this.payment = this.fb.group({
        consRate: 0,
        packageFee: [''],
        totalAmount: [''],
        paymentMode: ['Cash', Validators.required],
        eft: [''],
        cardtype: [''],
        cardName: [''],
        cardNo: [''],
        cardExpiry: [''],
        ddNo: [''],
        ddDate: [''],
        ddBank: [''],
      });
      this.getMasterData();
      this.getDocterMasterList();
      this.getPriceListApi();
      this.getCorporateList();
      this.disabled();
      this.general.get('pname').setValue(history.state.data.patient_name);
      this.general.get('uhid').setValue(history.state.data.uhid);
      this.general.get('age').setValue(history.state.data.age);
      this.general.get('ageType').setValue(history.state.data.agetype);
      this.general.get('mobileNo').setValue(history.state.data.mobile_primary);

      this.refByDocter = false;
      this.general.get('source').valueChanges.subscribe((value) => {
        // console.log(value, 'source value');
        if (value === 'Referred by Doctor') {
          this.refByDocter = true;
        } else {
          this.general.get('referredBy').setValue('');
          this.refByDocter = false;
        }
      });

      this.corporate.get('package').valueChanges.subscribe((value) => {
        // console.log(value);
        if (value) {
          // this.address.get('pincode').setValue(value.pin_code);
          this.payment.get('packageFee').setValue(value.rate);
          this.payment.get('totalAmount').setValue(value.rate);
        }
      });
      this.payment.get('consRate').valueChanges.subscribe((value) => {
        // console.log(value);
        if (value === null) {
          const total = +this.corporate.get('package').value.rate;
          this.payment.get('totalAmount').setValue(total);
        }
        if (value) {
          const packageFee = +this.corporate.get('package').value.rate;
          const total = packageFee - value;
          this.payment.get('totalAmount').setValue(total);
        }
      });
      this.payment.get('paymentMode').valueChanges.subscribe((value) => {
        // console.log(value);
        if (value) {
          const date = moment(new Date()).format('DD/MM/YYYY');
          this.payment.get('eft').setValue('');
          this.payment.get('cardtype').setValue('');
          this.payment.get('cardName').setValue('');
          this.payment.get('cardNo').setValue('');
          this.payment.get('cardExpiry').setValue(date);
          this.payment.get('ddNo').setValue('');
          this.payment.get('ddDate').setValue(date);
          this.payment.get('ddBank').setValue('');
        }
      });
      const date = moment(new Date()).format('DD/MM/YYYY');
      this.payment.get('cardExpiry').setValue(date);
      this.payment.get('ddDate').setValue(date);
      this.printBtn = false;
      this.payment.get('cardExpiry').valueChanges.subscribe((value) => {
        // console.log(value);

        const valueDate = moment(value, 'DD/MM/YYYY');
        // const valueDate = moment(value).format('DD/MM/YYYY');
        // console.log(valueDate, 'valueDate');
        const date = moment(new Date()).format('DD/MM/YYYY');
        const date2 = moment(date, 'DD/MM/YYYY');
        // console.log(date, 'date');
        const resultDate = moment(date2).diff(valueDate) > 0;
        if (resultDate) {
          this.snackBar.open('Not a Valid Date', 'x', {
            duration: 5000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
        // console.log(resultDate, 'resultDate');
      });
      this.payment.get('ddDate').valueChanges.subscribe((value) => {
        // console.log(value);

        const valueDate = moment(value, 'DD/MM/YYYY');
        // const valueDate = moment(value).format('DD/MM/YYYY');
        // console.log(valueDate, 'valueDate');
        const date = moment(new Date()).format('DD/MM/YYYY');
        const date2 = moment(date, 'DD/MM/YYYY');
        // console.log(date, 'date');
        const resultDate = moment(date2).diff(valueDate) > 0;
        if (resultDate) {
          this.snackBar.open('Not a Valid Date', 'x', {
            duration: 5000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
        // console.log(resultDate, 'resultDate');
      });
      this.corporate.get('corporateList').valueChanges.subscribe((value) => {
        // console.log(value);
        if (value === '') {
          this.commonService.getMasterDataAPI().subscribe(
            (data: any) => {
              console.log(data.result[0].row);
              const datas = data.result[0].row;
              // this.countryTypes = data.result[0].row;
              var groupBy = function (xs, key) {
                return xs.reduce(function (rv, x) {
                  (rv[x[key]] = rv[x[key]] || []).push(x);
                  return rv;
                }, {});
              };
              var groubedByTeam = groupBy(datas, 'mastertype');
              // this.patientCategoryTypes = groubedByTeam['Patient Category'];
              // this.sourceTypes = groubedByTeam['Source'];
              this.paymentModeTypes = groubedByTeam['Payment Mode'];
              // this.bankTypes = groubedByTeam['Bank'];
              setTimeout(() => {
                // console.log(this.payment.get('paymentMode').value, 'Payment');
                this.payment
                  .get('paymentMode')
                  .setValue(this.paymentModeTypes[1].mastervalue);
              }, 300);
              // console.log(groubedByTeam);
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.error.message);
            }
          );
          this.commonService.getPriceListDataAPI('0').subscribe(
            (data: any) => {
              // console.log(data.result[0].row);
              this.priceListTypes = data.result[0].row;
              this.corporate.get('priceList').setValue(this.priceListTypes[0]);
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.error.message);
            }
          );
        } else {
          if (value.party_name) {
            this.getPriceList(value);
          }
          this.paymentModeTypes = [
            {
              rowno: '10',
              mastertype: 'Payment Mode',
              masterid: '10116000000048',
              mastervalue: 'Cash',
            },
            {
              rowno: '12',
              mastertype: 'Payment Mode',
              masterid: '10116000000050',
              mastervalue: 'Credit',
            },
          ];
          setTimeout(() => {
            // console.log(this.payment.get('paymentMode').value, 'Payment');
            this.payment
              .get('paymentMode')
              .setValue(this.paymentModeTypes[1].mastervalue);
          }, 300);
        }
      });
      this.printBtn = false;
      this.saveBth = true;
      this.saveAndPrintBtn = true;
      this.corporate.get('physician').valueChanges.subscribe((value) => {
        console.log(value);
        // if (value === '') {
        //   this.general.get('package').setValue('');
        //   this.payment.get('packageFee').setValue('');
        // }
        if (value.doctor_name) {
          console.log(value);
          // const companyid = e.companyid;
          // const branchid = e.branchid;
          // const doctor_viewid = e.doctor_viewid;
          // const pricelisthdrid = this.corporate.get('priceList').value.pricelisthdrid;
          this.getPackageList(value);
          // this.address.get('pincode').setValue(value.pin_code);
        }
      });
      // this.filteredReferredBy = this.general
      //   .get('referredBy')
      //   .valueChanges.pipe(
      //     startWith<string | any>(''),
      //     map((value) =>
      //       typeof value === 'string' ? value : (<any>value).doctor_name
      //     ),
      //     map((referredBy) =>
      //       referredBy
      //         ? this._filterReferredBy(referredBy)
      //         : this.refDocterTypes.slice()
      //     )
      //   );
      // this.filteredPhysician = this.general.get('physician').valueChanges.pipe(
      //   startWith<string | any>(''),
      //   map((value) =>
      //     typeof value === 'string' ? value : (<any>value).doctor_name
      //   ),
      //   map((physician) =>
      //     physician
      //       ? this._filterphysician(physician)
      //       : this.attendingPhysicianTypes.slice()
      //   )
      // );
      // this.filteredCorporate = this.corporate
      //   .get('corporateList')
      //   .valueChanges.pipe(
      //     startWith<string | any>(''),
      //     map((value) =>
      //       typeof value === 'string' ? value : (<any>value).party_name
      //     ),
      //     map((corporate) =>
      //       corporate
      //         ? this._filterCorporate(corporate)
      //         : this.corporateListTypes.slice()
      //     )
      //   );
      this.corporate.get('priceList').valueChanges.subscribe((value) => {
        const physician = this.corporate.get('physician').value;
        if (physician !== '') {
          this.corporate.get('package').setValue('');
          this.payment.get('packageFee').setValue('');
          // this.myStepper.selectedIndex = 0;
          console.log(physician);
          this.getPackageList(physician);
          console.log(value.pricelisthdrid, 'priceList');
        }
        // const physician = this.general.get('physician').value;
        // // if (physician !== '') {
        // // this.general.get('package').setValue('');
        // this.payment.get('packageFee').setValue('');
        // console.log(physician);
        // this.getPackageList(value);
        // console.log(value.pricelisthdrid, 'priceList');
        // // }
        // // this.getConsultationList(value);
      });
    }
  }

  _keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  displayFnphysician(physician?: any): string | undefined {
    return physician ? physician.doctor_name : undefined;
  }
  displayFnCorporate(Corporate?: any): string | undefined {
    return Corporate ? Corporate.party_name : undefined;
  }
  private _filterphysician(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.attendingPhysicianTypes.filter((physician) =>
      physician.doctor_name.toLowerCase().includes(filterValue)
    );
  }
  private _filterCorporate(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.corporateListTypes.filter((corporate) =>
      corporate.party_name.toLowerCase().includes(filterValue)
    );
  }
  displayFnreferredBy(refDocter?: any): string | undefined {
    return refDocter ? refDocter.doctor_name : undefined;
  }
  private _filterReferredBy(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.refDocterTypes.filter((refDocter) =>
      refDocter.doctor_name.toLowerCase().includes(filterValue)
    );
  }

  public disabled() {
    this.general.controls['uhid'].disable();
    this.general.controls['opno'].disable();
    this.general.controls['pname'].disable();
    this.general.controls['age'].disable();
    this.general.controls['ageType'].disable();
    // this.opEpisode.controls['consultingFee'].disable();
    this.payment.controls['packageFee'].disable();
    this.payment.controls['totalAmount'].disable();
  }

  date2(e) {
    // console.log(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    // var value = datePipe.transform(e.value._d, 'dd-MM-yyyy');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.payment.get('cardExpiry').setValue(dateValue);
    // console.log(value);
  }
  date3(e) {
    // console.log(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    // var value = datePipe.transform(e.value._d, 'dd-MM-yyyy');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.payment.get('ddDate').setValue(dateValue);
    // console.log(value);
  }
  public async getMasterData() {
    await this.commonService.getMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        const datas = data.result[0].row;
        // this.countryTypes = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'mastertype');
        // this.patientCategoryTypes = groubedByTeam['Patient Category'];
        this.sourceTypes = groubedByTeam['Source'];
        this.paymentModeTypes = groubedByTeam['Payment Mode'];
        this.bankTypes = groubedByTeam['Bank'];
        // console.log(groubedByTeam);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getDocterMasterList() {
    await this.commonService.getDocterMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        const datas = data.result[0].row;
        this.refDocterTypes = data.result[0].row;
        this.filteredReferredBy = this.general
          .get('referredBy')
          .valueChanges.pipe(
            startWith<string | any>(''),
            map((value) =>
              typeof value === 'string' ? value : (<any>value).doctor_name
            ),
            map((referredBy) =>
              referredBy
                ? this._filterReferredBy(referredBy)
                : this.refDocterTypes.slice()
            )
          );
        // this.countryTypes = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'dtype');
        // console.log(groubedByTeam);
        this.attendingPhysicianTypes = groubedByTeam['In House'];
        this.filteredPhysician = this.corporate
          .get('physician')
          .valueChanges.pipe(
            startWith<string | any>(''),
            map((value) =>
              typeof value === 'string' ? value : (<any>value).doctor_name
            ),
            map((physician) =>
              physician
                ? this._filterphysician(physician)
                : this.attendingPhysicianTypes.slice()
            )
          );
        // this.consultationListTypes = data.result[0].row;
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getPackageList(e) {
    const companyid = e.companyid;
    const branchid = e.branchid;
    const pricelisthdrid = this.corporate.get('priceList').value.pricelisthdrid;
    // console.log(
    //   companyid,
    //   branchid,
    //   pricelisthdrid,
    //   'pricelisthdrid',
    //   'branchid',
    //   'doctor_viewid',
    //   'priceList'
    // );

    // if(companyid && branchid && doctor_viewid){
    console.log(this.corporate.get('priceList').value, 'priceList');
    // }
    await this.commonService
      .getPackageDataAPI(companyid, branchid, pricelisthdrid)
      .subscribe(
        (data: any) => {
          // console.log(data.result[0].row);
          this.packageTypes = data.result[0].row;
          if (this.corporate.get('package').value !== '') {
            const packageValue = this.corporate.get('package').value;
            console.log(packageValue, 'packageValue');
            console.log(this.packageTypes, 'packageTypes');
            this.packageTypes.map((value) => {
              if (value.packagename === packageValue.packagename) {
                if (value.rate === '') {
                  this.payment.controls['consRate'].disable();
                } else {
                  this.payment.controls['consRate'].enable();
                }
                const rate = value.rate;
                this.payment.get('packageFee').setValue(rate);
                this.payment.get('totalAmount').setValue(rate);
              }
            });
          }
          if (data.result[0].error) {
            this.snackBar.open(data.result[0].error.msg, 'x', {
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        },
        (error) => {
          console.log(error.error.message);
        }
      );
  }
  public getPackageFee(e) {
    // console.log(e.rate);
    if (e.rate === '') {
      this.payment.controls['consRate'].disable();
    } else {
      this.payment.controls['consRate'].enable();
    }
    const rate = e.rate;
    this.payment.get('packageFee').setValue(rate);
    this.payment.get('totalAmount').setValue(rate);
  }
  public async getCorporateList() {
    await this.commonService.getCorporateListDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.corporateListTypes = data.result[0].row;
        this.filteredCorporate = this.corporate
          .get('corporateList')
          .valueChanges.pipe(
            startWith<string | any>(''),
            map((value) =>
              typeof value === 'string' ? value : (<any>value).party_name
            ),
            map((corporate) =>
              corporate
                ? this._filterCorporate(corporate)
                : this.corporateListTypes.slice()
            )
          );
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public getPriceListApi() {
    const e = '0';
    // console.log(e);
    this.getPriceList(e);
  }
  public async getPriceList(e) {
    const corporateid = e.mg_partyhdrid || e;
    console.log(corporateid, 'corporateList');
    await this.commonService.getPriceListDataAPI(corporateid).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.priceListTypes = data.result[0].row;
        this.corporate.get('priceList').setValue(this.priceListTypes[0]);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  goBack(stepper: MatStepper) {
    stepper.previous();
  }

  goForward(stepper: MatStepper) {
    stepper.next();
  }
  async submit() {
    // const company = this.general.get('physician').value.companyid;
    // const companyid = this.general.get('physician').value.companyid;
    // const branchid = this.general.get('physician').value.branchid;
    const cardExpiry = this.payment.get('cardExpiry').value;
    const ddExpiry = this.payment.get('ddDate').value;
    if (cardExpiry && ddExpiry) {
      const value = this.payment.get('cardExpiry').value;
      const valueDate = moment(value, 'DD/MM/YYYY');
      const date = moment(new Date()).format('DD/MM/YYYY');
      const dateTest = moment(date, 'DD/MM/YYYY');
      const resultDate = moment(dateTest).diff(valueDate) > 0;

      const value2 = this.payment.get('ddDate').value;
      const valueDate2 = moment(value2, 'DD/MM/YYYY');
      const date2 = moment(new Date()).format('DD/MM/YYYY');
      const dateTest2 = moment(date2, 'DD/MM/YYYY');
      const resultDate2 = moment(dateTest2).diff(valueDate2) > 0;
      if (resultDate || resultDate2) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        const source = this.general.get('source').value;
        const referred_by_physician = this.general.get('referredBy').value
          .doctor_name;
        const uhid = this.general.get('uhid').value;
        const pname = this.general.get('pname').value;
        const mobileNo = this.general.get('mobileNo').value;
        const dob = history.state.data.date_of_birth_hidden;
        const mhc_package = this.corporate.get('package').value.packagename;
        const physician = this.corporate.get('physician').value.doctor_name;
        const company_name =
          this.corporate.get('corporateList').value.party_name || '';
        const priceList = this.corporate.get('priceList').value.pricelistname;
        const partner_company = '';
        const package_fee = this.payment.get('packageFee').value;
        const concession = this.payment.get('consRate').value || '';
        const paymentMode = this.payment.get('paymentMode').value;
        const concessionvalue = this.payment.get('consRate').value || '';
        const payment_amount = this.payment.get('totalAmount').value;
        const eft = this.payment.get('eft').value;
        const cardtype = this.payment.get('cardtype').value;
        const cardName = this.payment.get('cardName').value;
        const cardNo = this.payment.get('cardNo').value;
        const cardExpiry = this.payment.get('cardExpiry').value;
        const ddNo = this.payment.get('ddNo').value;
        const ddDate = this.payment.get('ddDate').value;
        const ddBank = this.payment.get('ddBank').value;

        console.log(
          // company,
          // companyid,
          // branchid,
          source,
          referred_by_physician,
          uhid,
          pname,
          mobileNo,
          dob,
          mhc_package,
          physician,
          company_name,
          priceList,
          partner_company,
          package_fee,
          concession,
          paymentMode,
          concessionvalue,
          payment_amount,
          eft,
          cardtype,
          cardName,
          cardNo,
          cardExpiry,
          ddNo,
          ddDate,
          ddBank
        );
        await this.commonService
          .saveMHCDataAPI(
            source,
            referred_by_physician,
            uhid,
            pname,
            mobileNo,
            dob,
            mhc_package,
            physician,
            company_name,
            priceList,
            partner_company,
            package_fee,
            concession,
            paymentMode,
            concessionvalue,
            payment_amount,
            eft,
            cardtype,
            cardName,
            cardNo,
            cardExpiry,
            ddNo,
            ddDate,
            ddBank
          )
          .subscribe(
            (data: any) => {
              // console.log(data.result[0].row);
              console.log(data);
              if (data.result[0].message) {
                const message = `OP No. is ${data.result[0].message[0]['OP No.']} , ${data.result[0].message[0].msg}`;
                this.snackBar.open(message, 'x', {
                  duration: 10000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.saveBth = false;
                this.saveAndPrintBtn = false;
              }
              // this.consultationListTypes = data.result[0].row;
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 5000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.message);
            }
          );
      }
    }
  }
  async submitAndContinue() {
    // const company = this.general.get('physician').value.companyid;
    // const companyid = this.general.get('physician').value.companyid;
    // const branchid = this.general.get('physician').value.branchid;
    const cardExpiry = this.payment.get('cardExpiry').value;
    const ddExpiry = this.payment.get('ddDate').value;
    if (cardExpiry && ddExpiry) {
      const value = this.payment.get('cardExpiry').value;
      const valueDate = moment(value, 'DD/MM/YYYY');
      const date = moment(new Date()).format('DD/MM/YYYY');
      const dateTest = moment(date, 'DD/MM/YYYY');
      const resultDate = moment(dateTest).diff(valueDate) > 0;

      const value2 = this.payment.get('ddDate').value;
      const valueDate2 = moment(value2, 'DD/MM/YYYY');
      const date2 = moment(new Date()).format('DD/MM/YYYY');
      const dateTest2 = moment(date2, 'DD/MM/YYYY');
      const resultDate2 = moment(dateTest2).diff(valueDate2) > 0;
      if (resultDate || resultDate2) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        const source = this.general.get('source').value;
        const referred_by_physician = this.general.get('referredBy').value
          .doctor_name;
        const uhid = this.general.get('uhid').value;
        const pname = this.general.get('pname').value;
        const mobileNo = this.general.get('mobileNo').value;
        const dob = history.state.data.date_of_birth_hidden;
        const mhc_package = this.corporate.get('package').value.packagename;
        const physician = this.corporate.get('physician').value.doctor_name;
        const company_name =
          this.corporate.get('corporateList').value.party_name || '';
        const priceList = this.corporate.get('priceList').value.pricelistname;
        const partner_company = '';
        const package_fee = this.payment.get('packageFee').value;
        const concession = this.payment.get('consRate').value || '';
        const paymentMode = this.payment.get('paymentMode').value;
        const concessionvalue = this.payment.get('consRate').value || '';
        const payment_amount = this.payment.get('totalAmount').value;
        const eft = this.payment.get('eft').value;
        const cardtype = this.payment.get('cardtype').value;
        const cardName = this.payment.get('cardName').value;
        const cardNo = this.payment.get('cardNo').value;
        const cardExpiry = this.payment.get('cardExpiry').value;
        const ddNo = this.payment.get('ddNo').value;
        const ddDate = this.payment.get('ddDate').value;
        const ddBank = this.payment.get('ddBank').value;

        console.log(
          // company,
          // companyid,
          // branchid,
          source,
          referred_by_physician,
          uhid,
          pname,
          mobileNo,
          dob,
          mhc_package,
          physician,
          company_name,
          priceList,
          partner_company,
          package_fee,
          concession,
          paymentMode,
          concessionvalue,
          payment_amount,
          eft,
          cardtype,
          cardName,
          cardNo,
          cardExpiry,
          ddNo,
          ddDate,
          ddBank
        );
        await this.commonService
          .saveMHCDataAPI(
            source,
            referred_by_physician,
            uhid,
            pname,
            mobileNo,
            dob,
            mhc_package,
            physician,
            company_name,
            priceList,
            partner_company,
            package_fee,
            concession,
            paymentMode,
            concessionvalue,
            payment_amount,
            eft,
            cardtype,
            cardName,
            cardNo,
            cardExpiry,
            ddNo,
            ddDate,
            ddBank
          )
          .subscribe(
            (data: any) => {
              // console.log(data.result[0].row);
              // console.log(data);
              if (data.result[0].message) {
                const message = `OP No. is ${data.result[0].message[0]['OP No.']} , ${data.result[0].message[0].msg}`;
                this.snackBar.open(message, 'x', {
                  duration: 10000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                const OPNo = data.result[0].message[0]['OP No.'];
                // console.log(OPNo);
                const OPNoValue = OPNo;
                const patientType = 'All';
                this.commonService
                  .getPatientListSearchDataAPI(patientType)
                  .subscribe(
                    (data: any) => {
                      // console.log(data.result[0].row);
                      // console.log(data.result[0].row);
                      const patienList = data.result[0].row;
                      const element = [];
                      patienList.map((list) => {
                        if (list.opn === OPNoValue) {
                          element.push(list);
                        }
                      });
                      // console.log(element, 'element');
                      if (element) {
                        this.router.navigate(['home/opservicebilling'], {
                          state: { data: element[0] },
                        });
                      }
                      // this.dataSource.paginator = this.paginator;
                      // this.dataSource.sort = this.sort;
                      if (data.result[0].error) {
                        this.snackBar.open(data.result[0].error.msg, 'x', {
                          duration: 5000,
                          horizontalPosition: this.horizontalPosition,
                          verticalPosition: this.verticalPosition,
                        });
                      }
                    },
                    (error) => {
                      console.log(error.message);
                    }
                  );
                this.saveBth = false;
              }
              // this.consultationListTypes = data.result[0].row;
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 5000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.message);
            }
          );
      }
    }
  }
  saveAndPrint() {
    // const company = this.general.get('physician').value.companyid;
    // const companyid = this.general.get('physician').value.companyid;
    // const branchid = this.general.get('physician').value.branchid;
    const source = this.general.get('source').value;
    const referred_by_physician = this.general.get('referredBy').value
      .doctor_name;
    const uhid = this.general.get('uhid').value;
    const pname = this.general.get('pname').value;
    const mobileNo = this.general.get('mobileNo').value;
    const dob = history.state.data.date_of_birth_hidden;
    const mhc_package = this.corporate.get('package').value.packagename;
    const physician = this.corporate.get('physician').value.doctor_name;
    const company_name =
      this.corporate.get('corporateList').value.party_name || '';
    const priceList = this.corporate.get('priceList').value.pricelistname;
    const partner_company = '';
    const package_fee = this.payment.get('packageFee').value;
    const concession = this.payment.get('consRate').value || '';
    const paymentMode = this.payment.get('paymentMode').value;
    const concessionvalue = this.payment.get('consRate').value || '';
    const payment_amount = this.payment.get('totalAmount').value;
    const eft = this.payment.get('eft').value;
    const cardtype = this.payment.get('cardtype').value;
    const cardName = this.payment.get('cardName').value;
    const cardNo = this.payment.get('cardNo').value;
    const cardExpiry = this.payment.get('cardExpiry').value;
    const ddNo = this.payment.get('ddNo').value;
    const ddDate = this.payment.get('ddDate').value;
    const ddBank = this.payment.get('ddBank').value;

    console.log(
      // company,
      // companyid,
      // branchid,
      source,
      referred_by_physician,
      uhid,
      pname,
      mobileNo,
      dob,
      mhc_package,
      physician,
      company_name,
      priceList,
      partner_company,
      package_fee,
      concession,
      paymentMode,
      concessionvalue,
      payment_amount,
      eft,
      cardtype,
      cardName,
      cardNo,
      cardExpiry,
      ddNo,
      ddDate,
      ddBank
    );
    this.commonService
      .saveMHCDataAPI(
        source,
        referred_by_physician,
        uhid,
        pname,
        mobileNo,
        dob,
        mhc_package,
        physician,
        company_name,
        priceList,
        partner_company,
        package_fee,
        concession,
        paymentMode,
        concessionvalue,
        payment_amount,
        eft,
        cardtype,
        cardName,
        cardNo,
        cardExpiry,
        ddNo,
        ddDate,
        ddBank
      )
      .subscribe(
        (data: any) => {
          // console.log(data.result[0].row);
          console.log(data);
          if (data.result[0].message) {
            if (data.result[0].message[0].recordid) {
              const id = data.result[0].message[0].recordid;
              console.log(id, 'id');
              this.printId = id;
              this.printBtn = true;
              this.print();
            }
            const message = `OP No. is ${data.result[0].message[0]['OP No.']} , ${data.result[0].message[0].msg}`;
            this.snackBar.open(message, 'x', {
              duration: 10000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.saveBth = false;
            this.saveAndPrintBtn = false;
          }
          // this.consultationListTypes = data.result[0].row;
          if (data.result[0].error) {
            this.snackBar.open(data.result[0].error.msg, 'x', {
              duration: 5000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        },
        (error) => {
          console.log(error.message);
        }
      );
  }
  print() {
    const id = this.printId;
    this.commonService.getPDFURL(id).subscribe(
      (data: any) => {
        console.log(data, 'data');
        console.log(data.result[0].row[0].pdffile, 'resId');
        const url =
          'https://agile-health.org/axpertwebx6/PDF/121212/121212.pdf';
        window.open(url);
      },
      (error) => {
        console.log(error.message);
      }
    );
  }
  reset() {
    this.myStepper.selectedIndex = 0;
    this.ngOnInit();
  }
}

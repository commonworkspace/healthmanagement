import { Routes } from '@angular/router';

import { LandingpageComponent } from './landingpage.component';
import { MhcregistrationComponent } from './mhcregistration/mhcregistration.component';
import { NewMhcregistrationComponent } from './new-mhcregistration/new-mhcregistration.component';
import { OpServiceBillingComponent } from './op-service-billing/op-service-billing.component';
import { OpregistractionComponent } from './opregistraction/opregistraction.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientSearchComponent } from './patient-search/patient-search.component';
import { VisitentryComponent } from './visitentry/visitentry.component';

export const LandingpageRoutes: Routes = [
  {
    path: '',
    component: LandingpageComponent,
  },
  {
    path: 'opregistration',
    component: OpregistractionComponent,
  },
  {
    path: 'newmhcregistration',
    component: NewMhcregistrationComponent,
  },
  {
    path: 'patientsearch',
    component: PatientSearchComponent,
  },
  {
    path: 'visitentry',
    component: VisitentryComponent,
  },
  {
    path: 'mhcregistration',
    component: MhcregistrationComponent,
  },
  {
    path: 'patientList',
    component: PatientListComponent,
  },
  {
    path: 'opservicebilling',
    component: OpServiceBillingComponent,
  },
];

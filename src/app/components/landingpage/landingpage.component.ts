import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'app/auth/session-storage.service';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.css'],
})
export class LandingpageComponent implements OnInit {
  navigationUrl: any[];
  constructor(
    private router: Router,
    private sessionService: SessionStorageService
  ) {}

  ngOnInit(): void {
    this.sessionService.tokenValidation();
    this.navigationUrl = [
      {
        id: 1,
        route: 'home/opregistration',
        title: 'OP Registration',
        image: 'assets/images/newOpReg.png',
        desp: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur accusantium recusandae totam.
      Culpa
      possimus perferendis, ab quasi debitis ipsa tenetur aut et necessitatibus, vero voluptatibus in
      numquam
      asperiores facere magnam?`,
      },
      {
        id: 2,
        route: 'home/newmhcregistration',
        title: 'New MHC Registration',
        image: 'assets/images/newMHCReg.png',
        desp: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur accusantium recusandae totam.
      Culpa
      possimus perferendis, ab quasi debitis ipsa tenetur aut et necessitatibus, vero voluptatibus in
      numquam
      asperiores facere magnam?`,
      },
      {
        id: 3,
        route: 'home/patientList',
        title: 'OP Patient List',
        image: 'assets/images/OpServiceBilling.png',
        desp: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur accusantium recusandae totam.
      Culpa
      possimus perferendis, ab quasi debitis ipsa tenetur aut et necessitatibus, vero voluptatibus in
      numquam
      asperiores facere magnam?`,
      },
      {
        id: 4,
        route: 'home/patientsearch',
        title: 'Patient Search',
        image: 'assets/images/QuickPatientSearch.png',
        desp: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur accusantium recusandae totam.
      Culpa
      possimus perferendis, ab quasi debitis ipsa tenetur aut et necessitatibus, vero voluptatibus in
      numquam
      asperiores facere magnam?`,
      },
      // {
      //   id: 7,
      //   route: 'home/opreceipt',
      //   title: 'OP Receipt',
      //   image: 'assets/images/OpReceipt.png',
      //   desp: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur accusantium recusandae totam.
      // Culpa
      // possimus perferendis, ab quasi debitis ipsa tenetur aut et necessitatibus, vero voluptatibus in
      // numquam
      // asperiores facere magnam?`,
      // },
    ];
  }
  navigate(nav: string) {
    this.router.navigate([`${nav}`]);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitentryComponent } from './visitentry.component';

describe('VisitentryComponent', () => {
  let component: VisitentryComponent;
  let fixture: ComponentFixture<VisitentryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitentryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitentryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

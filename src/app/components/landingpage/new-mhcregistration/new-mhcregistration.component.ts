import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  // FormControl,
} from '@angular/forms';
import { SessionStorageService } from 'app/auth/session-storage.service';
import { CommonService } from 'app/services/common.service';
import { MatDatepicker } from '@angular/material/datepicker';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import * as moment from 'moment';
import { Router } from '@angular/router';

export interface Country {
  rowno: string;
  column1: string;
  country_sname: string;
  country_name: string;
  active: string;
  column2: string;
  recordid: string;
}
export interface State {
  rowno: string;
  column1: string;
  stateid: string;
  state_sname: string;
  state_name: string;
  country_name: string;
  active: string;
  column2: string;
}
export interface District {
  rowno: string;
  column1: string;
  cityid: string;
  city_sname: string;
  city_name: string;
  country_name: string;
  state_name: string;
  active: string;
  column2: string;
}

@Component({
  selector: 'app-new-mhcregistration',
  templateUrl: './new-mhcregistration.component.html',
  styleUrls: ['./new-mhcregistration.component.css'],
})
export class NewMhcregistrationComponent implements OnInit {
  @ViewChild('picker') picker: MatDatepicker<Date>;
  @ViewChild('picker') picker2: MatDatepicker<Date>;
  @ViewChild('picker') picker3: MatDatepicker<Date>;
  @ViewChild('stepper') private myStepper: MatStepper;
  totalStepsCount: number;
  general: FormGroup;
  address: FormGroup;
  corporate: FormGroup;
  // opEpisode: FormGroup;
  payment: FormGroup;
  public patientCategoryTypes: any = [];
  public sltTypes: any = [];
  public genderTypes: any = [];
  public sourceTypes: any = [];
  public refDocterTypes: any = [];
  public refByDocter: boolean = false;
  public countryTypes: Country[] = [];
  public stateTypes: State[] = [];
  public districtTypes: any = [];
  public pincodeTypes: any = [];
  public priceListTypes: any = [];
  public corporateListTypes: any = [];
  public consultationListTypes: any = [];
  public paymentModeTypes: any = [];
  public attendingPhysicianTypes: any = [];
  public packageTypes: any = [];
  public bankTypes: any = [];
  public rentDetail = new FormControl('selectedDate');
  public minDate = new Date(1920, 1, 1);
  public maxDate = new Date();
  public expiredMinDate = new Date();
  public expiredMaxDate = new Date(2500, 1, 1);
  public ddexpiredMinDate = new Date();
  public ddexpiredMaxDate = new Date(2500, 1, 1);

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  filteredEmployee: Observable<Country[]>;
  filteredState: Observable<State[]>;
  filteredDistrict: Observable<District[]>;
  filteredArea: Observable<any[]>;
  filteredPhysician: Observable<any[]>;
  filteredCorporate: Observable<any[]>;
  filteredReferredBy: Observable<any[]>;

  printBtn: boolean = false;
  saveBth: boolean = true;
  saveAndPrintBtn: boolean = true;
  public printId = '';

  constructor(
    private fb: FormBuilder,
    private sessionService: SessionStorageService,
    private commonService: CommonService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sessionService.tokenValidation();
    this.general = this.fb.group({
      category: ['General', Validators.required],
      source: ['Walk In', Validators.required],
      referredBy: [''],
      uhid: ['Auto', Validators.required],
      gender: ['', Validators.required],
      slt: ['', Validators.required],
      pname: ['', Validators.required],
      fathername: ['', Validators.required],
      setDate: [''],
      dob: [''],
      age: ['', Validators.compose([Validators.required, Validators.max(150)])],
      ageType: ['Years', Validators.required],
    });
    this.address = this.fb.group({
      country: ['', Validators.required],
      state: ['', Validators.required],
      district: ['', Validators.required],
      pincode: ['', Validators.required],
      area: ['', Validators.required],
      fullAddress: ['', Validators.required],
      mobileNo: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('[6-9]\\d{9}'),
        ]),
      ],
    });
    this.corporate = this.fb.group({
      corporateList: [''],
      priceList: [''],
      physician: ['', Validators.required],
      package: ['', Validators.required],
    });
    // this.opEpisode = this.fb.group({
    //   // physician: ['', Validators.required],
    //   consultationType: ['', Validators.required],
    //   consultingFee: [''],
    // });
    this.payment = this.fb.group({
      registrationFee: [''],
      consRate: 0,
      packageFee: [''],
      totalAmount: [''],
      paymentMode: ['Cash', Validators.required],
      eft: [''],
      cardtype: [''],
      cardName: [''],
      cardNo: [''],
      cardExpiry: [''],
      ddNo: [''],
      ddDate: [''],
      ddBank: [''],
    });

    // get API
    this.getMasterData();
    this.getPriceListApi();
    this.getDocterMasterList();
    this.disabled();
    this.address.get('country').setValue({
      rowno: '91',
      column1: '91',
      country_sname: 'IN',
      country_name: 'INDIA',
      active: 'True',
      column2: 'Edit',
      recordid: '11072000000091',
    });
    this.address.get('state').setValue({
      rowno: '31',
      column1: '31',
      stateid: '11898000000000',
      state_sname: 'TN',
      state_name: 'Tamil Nadu',
      country_name: 'INDIA',
      active: 'True',
      column2: 'Edit',
    });
    this.address.get('district').setValue({
      rowno: '2',
      column1: '2',
      cityid: '16085011004149',
      city_sname: 'T5139',
      city_name: 'Chennai',
      country_name: 'INDIA',
      state_name: 'Tamil Nadu',
      active: 'True',
      column2: 'Edit',
    });
    this.refByDocter = false;
    this.general.get('source').valueChanges.subscribe((value) => {
      // console.log(value, 'source value');
      if (value === 'Referred by Doctor') {
        this.refByDocter = true;
      } else {
        this.general.get('referredBy').setValue('');
        this.refByDocter = false;
      }
    });
    this.general.get('gender').valueChanges.subscribe((value) => {
      // console.log(value);
      const stl = this.sltTypes;
      let result = [];
      if (value === 'Male') {
        for (let stlMale of stl) {
          if (
            stlMale.mastervalue === 'Baby' ||
            stlMale.mastervalue === 'Dr.' ||
            stlMale.mastervalue === 'Master.' ||
            stlMale.mastervalue === 'Mr.'
          ) {
            result.push(stlMale);
          }
        }
        // console.log(result, 'maleresult');
        this.genderTypes = result;
        this.general.get('slt').setValue(this.genderTypes[3].mastervalue);
      }
      if (value === 'Female') {
        result.length = 0;
        for (let stlMale of stl) {
          if (
            stlMale.mastervalue === 'Baby' ||
            stlMale.mastervalue === 'Dr' ||
            stlMale.mastervalue === 'Miss.' ||
            stlMale.mastervalue === 'Ms.'
          ) {
            result.push(stlMale);
          }
        }
        // console.log(result, 'maleresult');
        this.genderTypes = result;
        this.general.get('slt').setValue(this.genderTypes[2].mastervalue);
      }
      if (value === 'Transgender') {
        result.length = 0;
        for (let stlMale of stl) {
          if (stlMale.mastervalue === 'Mx.') {
            result.push(stlMale);
          }
        }
        // console.log(result, 'maleresult');
        this.genderTypes = result;
        this.general.get('slt').setValue(this.genderTypes[0].mastervalue);
      }
      // this.genderTypes = result;
    });

    this.address.get('country').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value.country_name) {
        this.getCountryNames(value.country_name);
      }
    });
    this.address.get('district').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value.city_name) {
        this.getDistrictNames(value.city_name);
      }
    });
    this.address.get('state').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value.state_name) {
        this.getStateNames(value.state_name);
      }
    });
    this.address.get('area').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value) {
        this.address.get('pincode').setValue(value.pin_code);
      }
    });
    this.corporate.get('physician').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value.doctor_name) {
        console.log(value);
        // const companyid = e.companyid;
        // const branchid = e.branchid;
        // const doctor_viewid = e.doctor_viewid;
        // const pricelisthdrid = this.corporate.get('priceList').value.pricelisthdrid;
        this.getPackageList(value);
        // this.address.get('pincode').setValue(value.pin_code);
      }
    });
    this.corporate.get('package').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value) {
        // this.address.get('pincode').setValue(value.pin_code);
        this.payment.get('packageFee').setValue(value.rate);
        this.payment.get('totalAmount').setValue(value.rate);
      }
    });
    this.payment.get('consRate').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value === null) {
        const total = +this.corporate.get('package').value.rate;
        this.payment.get('totalAmount').setValue(total);
      }
      if (value) {
        const packageFee = +this.corporate.get('package').value.rate;
        const total = packageFee - value;
        this.payment.get('totalAmount').setValue(total);
      }
    });
    this.payment.get('paymentMode').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value) {
        const date = moment(new Date()).format('DD/MM/YYYY');
        this.payment.get('eft').setValue('');
        this.payment.get('cardtype').setValue('');
        this.payment.get('cardName').setValue('');
        this.payment.get('cardNo').setValue('');
        this.payment.get('cardExpiry').setValue(date);
        this.payment.get('ddNo').setValue('');
        this.payment.get('ddDate').setValue(date);
        this.payment.get('ddBank').setValue('');
      }
    });
    const date = moment(new Date()).format('DD/MM/YYYY');
    this.payment.get('cardExpiry').setValue(date);
    this.payment.get('ddDate').setValue(date);
    this.printBtn = false;
    this.payment.get('cardExpiry').valueChanges.subscribe((value) => {
      // console.log(value);
      // this.payment.get('cardExpiry').setValue(value);
      const valueDate = moment(value, 'DD/MM/YYYY');
      // const valueDate = moment(value).format('DD/MM/YYYY');
      // console.log(valueDate, 'valueDate');
      const date = moment(new Date()).format('DD/MM/YYYY');
      const date2 = moment(date, 'DD/MM/YYYY');
      // console.log(date, 'date');
      const resultDate = moment(date2).diff(valueDate) > 0;
      if (resultDate) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      // console.log(resultDate, 'resultDate');
    });
    this.payment.get('ddDate').valueChanges.subscribe((value) => {
      // console.log(value);
      // this.payment.get('ddDate').setValue(value);
      const valueDate = moment(value, 'DD/MM/YYYY');
      // const valueDate = moment(value).format('DD/MM/YYYY');
      // console.log(valueDate, 'valueDate');
      const date = moment(new Date()).format('DD/MM/YYYY');
      const date2 = moment(date, 'DD/MM/YYYY');
      // console.log(date, 'date');
      const resultDate = moment(date2).diff(valueDate) > 0;
      if (resultDate) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      // console.log(resultDate, 'resultDate');
    });
    this.corporate.get('corporateList').valueChanges.subscribe((value) => {
      // console.log(value);
      if (value === '') {
        this.commonService.getMasterDataAPI().subscribe(
          (data: any) => {
            console.log(data.result[0].row);
            const datas = data.result[0].row;
            // this.countryTypes = data.result[0].row;
            var groupBy = function (xs, key) {
              return xs.reduce(function (rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
              }, {});
            };
            var groubedByTeam = groupBy(datas, 'mastertype');
            // this.patientCategoryTypes = groubedByTeam['Patient Category'];
            // this.sourceTypes = groubedByTeam['Source'];
            this.paymentModeTypes = groubedByTeam['Payment Mode'];
            // this.bankTypes = groubedByTeam['Bank'];
            setTimeout(() => {
              // console.log(this.payment.get('paymentMode').value, 'Payment');
              this.payment
                .get('paymentMode')
                .setValue(this.paymentModeTypes[1].mastervalue);
            }, 300);
            // console.log(groubedByTeam);
            if (data.result[0].error) {
              this.snackBar.open(data.result[0].error.msg, 'x', {
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          },
          (error) => {
            console.log(error.error.message);
          }
        );
        this.commonService.getPriceListDataAPI('0').subscribe(
          (data: any) => {
            // console.log(data.result[0].row);
            this.priceListTypes = data.result[0].row;
            this.corporate.get('priceList').setValue(this.priceListTypes[0]);
            if (data.result[0].error) {
              this.snackBar.open(data.result[0].error.msg, 'x', {
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          },
          (error) => {
            console.log(error.error.message);
          }
        );
      } else {
        if (value.party_name) {
          this.getPriceList(value);
        }
        this.paymentModeTypes = [
          {
            rowno: '10',
            mastertype: 'Payment Mode',
            masterid: '10116000000048',
            mastervalue: 'Cash',
          },
          {
            rowno: '12',
            mastertype: 'Payment Mode',
            masterid: '10116000000050',
            mastervalue: 'Credit',
          },
        ];
        setTimeout(() => {
          // console.log(this.payment.get('paymentMode').value, 'Payment');
          this.payment
            .get('paymentMode')
            .setValue(this.paymentModeTypes[1].mastervalue);
        }, 300);
      }
    });
    this.corporate.get('priceList').valueChanges.subscribe((value) => {
      console.log(value);
      const physician = this.corporate.get('physician').value;
      if (physician !== '') {
        this.corporate.get('package').setValue('');
        this.payment.get('packageFee').setValue('');
        // this.myStepper.selectedIndex = 0;
        console.log(physician);
        this.getPackageList(physician);
        console.log(value.pricelisthdrid, 'priceList');
      }
      // this.getConsultationList(value);
    });
    this.printBtn = false;
    this.saveBth = true;
    this.saveAndPrintBtn = true;
  }

  onStepChange(e) {
    // this.getAddressApis();
    // console.log(e);
    if (e.selectedIndex === 1) {
      if (
        this.address.get('area').value === '' ||
        this.address.get('country').value === '' ||
        this.address.get('state').value === '' ||
        this.address.get('district').value === ''
      ) {
        this.getAddressApis();
      }
      if (this.general.get('setDate').value.length === 0) {
        if (this.general.get('ageType').value === 'Years') {
          var d = new Date();
          // console.log(d);
          const age = this.general.get('age').value;
          d.setFullYear(d.getFullYear() - age);
          // console.log(moment(d).format('DD/MM/YYYY'), 'toLocaleDateString');
          const date = moment(d).format('DD/MM/YYYY');
          this.general.get('setDate').setValue(d);
          this.general.get('dob').setValue(date);
          // const age = this.general.get('age').value;
          // const currentYear = new Date().getFullYear();
          // const returnYear = currentYear - age;
          // const currentDate = moment(new Date()).format('DD/MM/YYYY');
          // console.log(returnYear, 'returnYear', currentDate, 'currentDate');
          // const words = currentDate.split('/');
          // const date = `${words[0]}/${words[1]}/${returnYear}`;
          // console.log(date);
          // this.general.get('dob').setValue(date);
        }
        if (this.general.get('ageType').value === 'Months') {
          var d = new Date();
          // console.log(d);
          const age = this.general.get('age').value;
          d.setMonth(d.getMonth() - age);
          // console.log(moment(d).format('DD/MM/YYYY'), 'toLocaleDateString');
          const date = moment(d).format('DD/MM/YYYY');
          this.general.get('setDate').setValue(d);
          this.general.get('dob').setValue(date);
        }
        if (this.general.get('ageType').value === 'Days') {
          var d = new Date();
          // console.log(d);
          const age = this.general.get('age').value;
          d.setDate(d.getDate() - age);
          // console.log(moment(d).format('DD/MM/YYYY'), 'toLocaleDateString');
          const date = moment(d).format('DD/MM/YYYY');
          this.general.get('setDate').setValue(d);
          this.general.get('dob').setValue(date);
        }
      }
    }
  }
  _keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  goBack(stepper: MatStepper) {
    stepper.previous();
  }

  goForward(stepper: MatStepper) {
    stepper.next();
    // const indexStep = stepper.selectedIndex;
    // // stepper._steps.length
    // // console.log(indexStep);
    // if (indexStep === 1) {
    //   if (
    //     this.address.get('area').value === '' ||
    //     this.address.get('country').value === '' ||
    //     this.address.get('state').value === '' ||
    //     this.address.get('district').value === ''
    //   ) {
    //     this.getAddressApis();
    //   }
    //   if (this.general.get('setDate').value.length === 0) {
    //     if (this.general.get('ageType').value === 'Years') {
    //       var d = new Date();
    //       // console.log(d);
    //       const age = this.general.get('age').value;
    //       d.setFullYear(d.getFullYear() - age);
    //       // console.log(moment(d).format('DD/MM/YYYY'), 'toLocaleDateString');
    //       const date = moment(d).format('DD/MM/YYYY');
    //       this.general.get('setDate').setValue(d);
    //       this.general.get('dob').setValue(date);
    //       // const age = this.general.get('age').value;
    //       // const currentYear = new Date().getFullYear();
    //       // const returnYear = currentYear - age;
    //       // const currentDate = moment(new Date()).format('DD/MM/YYYY');
    //       // console.log(returnYear, 'returnYear', currentDate, 'currentDate');
    //       // const words = currentDate.split('/');
    //       // const date = `${words[0]}/${words[1]}/${returnYear}`;
    //       // console.log(date);
    //       // this.general.get('dob').setValue(date);
    //     }
    //     if (this.general.get('ageType').value === 'Months') {
    //       var d = new Date();
    //       // console.log(d);
    //       const age = this.general.get('age').value;
    //       d.setMonth(d.getMonth() - age);
    //       // console.log(moment(d).format('DD/MM/YYYY'), 'toLocaleDateString');
    //       const date = moment(d).format('DD/MM/YYYY');
    //       this.general.get('setDate').setValue(d);
    //       this.general.get('dob').setValue(date);
    //     }
    //     if (this.general.get('ageType').value === 'Days') {
    //       var d = new Date();
    //       // console.log(d);
    //       const age = this.general.get('age').value;
    //       d.setDate(d.getDate() - age);
    //       // console.log(moment(d).format('DD/MM/YYYY'), 'toLocaleDateString');
    //       const date = moment(d).format('DD/MM/YYYY');
    //       this.general.get('setDate').setValue(d);
    //       this.general.get('dob').setValue(date);
    //     }
    //   }
    // }
  }
  displayFn(country?: Country): string | undefined {
    return country ? country.country_name : undefined;
  }
  displayFnState(state?: State): string | undefined {
    return state ? state.state_name : undefined;
  }
  displayFnDistrict(district?: District): string | undefined {
    return district ? district.city_name : undefined;
  }
  displayFnArea(area?: any): string | undefined {
    return area ? area.pin_description : undefined;
  }
  displayFnphysician(physician?: any): string | undefined {
    return physician ? physician.doctor_name : undefined;
  }
  displayFnCorporate(Corporate?: any): string | undefined {
    return Corporate ? Corporate.party_name : undefined;
  }
  private _filterEmployees(value: string): Country[] {
    const filterValue = value.toLowerCase();

    return this.countryTypes.filter((state) =>
      state.country_name.toLowerCase().includes(filterValue)
    );
  }
  private _filterState(value: string): State[] {
    const filterValue = value.toLowerCase();

    return this.stateTypes.filter((state) =>
      state.state_name.toLowerCase().includes(filterValue)
    );
  }
  private _filterDistrict(value: string): District[] {
    const filterValue = value.toLowerCase();

    return this.districtTypes.filter((state) =>
      state.city_name.toLowerCase().includes(filterValue)
    );
  }

  private _filterArea(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.pincodeTypes.filter((area) =>
      area.pin_description.toLowerCase().includes(filterValue)
    );
  }
  private _filterphysician(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.attendingPhysicianTypes.filter((physician) =>
      physician.doctor_name.toLowerCase().includes(filterValue)
    );
  }
  private _filterCorporate(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.corporateListTypes.filter((corporate) =>
      corporate.party_name.toLowerCase().includes(filterValue)
    );
  }
  displayFnreferredBy(refDocter?: any): string | undefined {
    return refDocter ? refDocter.doctor_name : undefined;
  }
  private _filterReferredBy(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.refDocterTypes.filter((refDocter) =>
      refDocter.doctor_name.toLowerCase().includes(filterValue)
    );
  }
  public getPatientCategory(e) {
    // console.log(e);
    if (e === 'Corporate') {
      console.log(e);
      this.getCorporateList();
      this.corporate.controls['corporateList'].enable();
    } else {
      this.corporateListTypes = [];
      this.corporate.controls['corporateList'].disable();
      this.corporate.controls.corporateList.setValue('');
      this.getPriceListApi();
    }
  }

  // disable functionality
  public disabled() {
    this.general.controls['uhid'].disable();
    this.address.controls['pincode'].disable();
    this.corporate.controls['corporateList'].disable();
    // this.opEpisode.controls['consultingFee'].disable();
    this.payment.controls['packageFee'].disable();
    this.payment.controls['registrationFee'].disable();
    this.payment.controls['totalAmount'].disable();
  }
  // date picker
  showPicker(): void {
    this.picker.open();
  }
  date(e) {
    // console.log(e);
    this.general.get('setDate').setValue(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.general.get('dob').setValue(dateValue);
    // console.log(value);
    const { yearDiff, monthDiff, dayDiff } = this.dateDiff(value, new Date());
    // console.log(yearDiff, monthDiff, dayDiff, 'dateResult');
    if (yearDiff > 0) {
      // console.log(yearDiff);
      this.general.get('age').setValue(yearDiff);
      const year = 'Years';
      this.general.get('ageType').setValue(year);
      return;
    }
    if (monthDiff > 0) {
      // console.log(monthDiff);
      this.general.get('age').setValue(monthDiff);
      const month = 'Months';
      this.general.get('ageType').setValue(month);
      return;
    }
    if (dayDiff >= 0) {
      if (dayDiff === 1) {
        const dayDiffCurrect = dayDiff;
        this.general.get('age').setValue(dayDiffCurrect);
        const day = 'Days';
        this.general.get('ageType').setValue(day);
        return;
      }
      const dayDiffCurrect = dayDiff - 1;
      this.general.get('age').setValue(dayDiffCurrect);
      const day = 'Days';
      this.general.get('ageType').setValue(day);
      return;
    }
  }

  date2(e) {
    // console.log(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    // var value = datePipe.transform(e.value._d, 'dd-MM-yyyy');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.payment.get('cardExpiry').setValue(dateValue);
    // console.log(value);
  }
  date3(e) {
    // console.log(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    // var value = datePipe.transform(e.value._d, 'dd-MM-yyyy');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.payment.get('ddDate').setValue(dateValue);
    // console.log(value);
  }
  dateDiff(startingDate, endingDate) {
    var startDate = new Date(
      new Date(startingDate).toISOString().substr(0, 10)
    );
    if (!endingDate) {
      endingDate = new Date().toISOString().substr(0, 10); // need date in YYYY-MM-DD format
    }
    var endDate = new Date(endingDate);
    if (startDate > endDate) {
      var swap = startDate;
      startDate = endDate;
      endDate = swap;
    }
    var startYear = startDate.getFullYear();
    var february =
      (startYear % 4 === 0 && startYear % 100 !== 0) || startYear % 400 === 0
        ? 29
        : 28;
    var daysInMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var yearDiff = endDate.getFullYear() - startYear;
    var monthDiff = endDate.getMonth() - startDate.getMonth();
    if (monthDiff < 0) {
      yearDiff--;
      monthDiff += 12;
    }
    var dayDiff = endDate.getDate() - startDate.getDate();
    if (dayDiff < 0) {
      if (monthDiff > 0) {
        monthDiff--;
      } else {
        yearDiff--;
        monthDiff = 11;
      }
      dayDiff += daysInMonth[startDate.getMonth()];
    }
    return { yearDiff, monthDiff, dayDiff };
  }
  public async getMasterData() {
    await this.commonService.getMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        const datas = data.result[0].row;
        // this.countryTypes = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'mastertype');
        this.patientCategoryTypes = groubedByTeam['Patient Category'];
        this.sourceTypes = groubedByTeam['Source'];
        this.paymentModeTypes = groubedByTeam['Payment Mode'];
        this.sltTypes = groubedByTeam['Salutation'];
        this.bankTypes = groubedByTeam['Bank'];
        console.log(groubedByTeam);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  // public getGender(e) {
  //   console.log(e, 'event');
  //   if (e === 'Male') {
  //     this.general.get('slt').setValue('Mr.');
  //   }
  //   if (e === 'Female') {
  //     this.general.get('slt').setValue('Ms.');
  //   }
  //   if (e === 'Transgender') {
  //     this.general.get('slt').setValue('Mx.');
  //   }
  // }
  //
  public getAddressApis() {
    const dob = this.general.get('dob').value;
    if (dob === '') {
      const ageType = this.general.get('ageType').value;
      if (ageType === 'year') {
        const date = new Date();
        const age = +this.general.get('age').value;
        const year = date.getFullYear();
        const calculate = year - age;
        const day = date.getDate();
        const month = date.getMonth() + 1;
        // console.log(`${day}/${month}/${calculate}`);
        const result = `${day}/${month}/${calculate}`;
        this.general.get('dob').setValue(result);
      }
    }
    this.getCountry();
    // this.getState();
    // this.getDistrict();
    // this.getArea();
  }
  public async getCountry() {
    await this.commonService.getCountryDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.countryTypes = data.result[0].row;
        this.filteredEmployee = this.address.get('country').valueChanges.pipe(
          startWith<string | Country>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).country_name
          ),
          map((country) =>
            country ? this._filterEmployees(country) : this.countryTypes.slice()
          )
        );
        this.getState();
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  // displayFn(subject) {
  //   return subject ? subject.country_name : undefined;
  // }
  // public filterValue(value: string) {
  //   const filteredValue = value.toLowerCase();
  //   return this.countryTypes.filter((option) =>
  //     option.toLowerCase().includes(filteredValue)
  //   );
  // }
  public async getState() {
    const e = this.address.get('country').value.country_name;
    await this.commonService.getStateDataAPI(e).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.stateTypes = data.result[0].row;
        this.filteredState = this.address.get('state').valueChanges.pipe(
          startWith<string | State>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).state_name
          ),
          map((state) =>
            state ? this._filterState(state) : this.stateTypes.slice()
          )
        );
        this.getDistrict();
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getDistrict() {
    const country = this.address.get('country').value;
    const state = this.address.get('state').value;
    await this.commonService.getDistrictDataAPI(state, country).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.districtTypes = data.result[0].row;
        this.filteredDistrict = this.address.get('district').valueChanges.pipe(
          startWith<string | District>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).city_name
          ),
          map((district) =>
            district
              ? this._filterDistrict(district)
              : this.districtTypes.slice()
          )
        );
        this.getArea();
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getCountryNames(e) {
    // console.log(e, 'event');
    await this.commonService.getStateDataAPI(e).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.stateTypes = data.result[0].row;
        this.filteredState = this.address.get('state').valueChanges.pipe(
          startWith<string | State>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).state_name
          ),
          map((state) =>
            state ? this._filterState(state) : this.stateTypes.slice()
          )
        );
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getStateNames(e) {
    // console.log(e, 'event');
    const country = this.address.get('country').value;
    // console.log(country);
    this.districtTypes = [];
    this.address.get('district').setValue('');
    this.commonService.getDistrictDataAPI(e, country).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.districtTypes = data.result[0].row;
        this.filteredDistrict = this.address.get('district').valueChanges.pipe(
          startWith<string | District>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).city_name
          ),
          map((district) =>
            district
              ? this._filterDistrict(district)
              : this.districtTypes.slice()
          )
        );
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  // public onPincodeSearch(e) {
  //   // console.log(e.length, 'e');
  //   if (e.length >= 6) {
  //     const pincode = e.toString();
  //     const country = this.address.get('country').value;
  //     const state = this.address.get('state').value;
  //     const district = this.address.get('district').value;
  //     // console.log(country, state, district);
  //     this.commonService
  //       .getPincodeDataAPI(pincode, country, state, district)
  //       .subscribe(
  //         (data: any) => {
  //           // console.log(data.result[0].row);
  //           this.pincodeTypes = data.result[0].row;
  //         },
  //         (error) => {
  //           console.log(error.error.message);
  //         }
  //       );
  //   }
  // }
  public async getArea() {
    const country = this.address.get('country').value.country_name;
    const state = this.address.get('state').value.state_name;
    const district = this.address.get('district').value.city_name;
    // console.log(country, state, district);
    await this.commonService.getAreaDataAPI(country, state, district).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.pincodeTypes = data.result[0].row;
        this.filteredArea = this.address.get('area').valueChanges.pipe(
          startWith<string | any>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).pin_description
          ),
          map((area) =>
            area ? this._filterArea(area) : this.pincodeTypes.slice()
          )
        );
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getDistrictNames(e) {
    console.log(e, 'event');
    const country = this.address.get('country').value.country_name;
    const state = this.address.get('state').value.state_name;
    // const country = this.address.get('country').value;
    console.log(country, state, e);
    this.pincodeTypes = [];
    this.address.get('area').setValue('');
    await this.commonService.getAreaDataAPI(country, state, e).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.pincodeTypes = data.result[0].row;
        this.filteredArea = this.address.get('area').valueChanges.pipe(
          startWith<string | any>(''),
          map((value) =>
            typeof value === 'string' ? value : (<any>value).pin_description
          ),
          map((area) =>
            area ? this._filterArea(area) : this.pincodeTypes.slice()
          )
        );
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public getPincode(e) {
    this.address.get('pincode').setValue(e.pin_code);
  }
  public getPriceListApi() {
    const e = '0';
    // console.log(e);
    this.getPriceList(e);
  }
  public async getPriceList(e) {
    const corporateid = e.mg_partyhdrid || e;
    console.log(corporateid, 'corporateList');
    await this.commonService.getPriceListDataAPI(corporateid).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.priceListTypes = data.result[0].row;
        this.corporate.get('priceList').setValue(this.priceListTypes[0]);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getCorporateList() {
    await this.commonService.getCorporateListDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.corporateListTypes = data.result[0].row;
        this.filteredCorporate = this.corporate
          .get('corporateList')
          .valueChanges.pipe(
            startWith<string | any>(''),
            map((value) =>
              typeof value === 'string' ? value : (<any>value).party_name
            ),
            map((corporate) =>
              corporate
                ? this._filterCorporate(corporate)
                : this.corporateListTypes.slice()
            )
          );
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getPackageList(e) {
    const companyid = e.companyid;
    const branchid = e.branchid;
    // const doctor_viewid = e.doctor_viewid;
    const pricelisthdrid = this.corporate.get('priceList').value.pricelisthdrid;
    // console.log(
    //   companyid,
    //   branchid,
    //   // doctor_viewid,
    //   pricelisthdrid,
    //   'pricelisthdrid',
    //   'branchid',
    //   'doctor_viewid',
    //   'priceList'
    // );

    // if(companyid && branchid && doctor_viewid){
    // console.log(pricelisthdrid, 'priceList');
    // }
    await this.commonService
      .getPackageDataAPI(companyid, branchid, pricelisthdrid)
      .subscribe(
        (data: any) => {
          // console.log(data.result[0].row);
          this.packageTypes = data.result[0].row;
          if (data.result[0].error) {
            this.snackBar.open(data.result[0].error.msg, 'x', {
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        },
        (error) => {
          console.log(error.error.message);
        }
      );
  }
  public getPackageFee(e) {
    // console.log(e.rate);
    if (e.rate === '') {
      this.payment.controls['consRate'].disable();
    } else {
      this.payment.controls['consRate'].enable();
    }
    const rate = e.rate;
    this.payment.get('packageFee').setValue(rate);
    this.payment.get('totalAmount').setValue(rate);
  }
  public async getDocterMasterList() {
    await this.commonService.getDocterMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        const datas = data.result[0].row;
        this.refDocterTypes = data.result[0].row;
        this.filteredReferredBy = this.general
          .get('referredBy')
          .valueChanges.pipe(
            startWith<string | any>(''),
            map((value) =>
              typeof value === 'string' ? value : (<any>value).doctor_name
            ),
            map((referredBy) =>
              referredBy
                ? this._filterReferredBy(referredBy)
                : this.refDocterTypes.slice()
            )
          );
        // this.countryTypes = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'dtype');
        // console.log(groubedByTeam);
        this.attendingPhysicianTypes = groubedByTeam['In House'];
        this.filteredPhysician = this.corporate
          .get('physician')
          .valueChanges.pipe(
            startWith<string | any>(''),
            map((value) =>
              typeof value === 'string' ? value : (<any>value).doctor_name
            ),
            map((physician) =>
              physician
                ? this._filterphysician(physician)
                : this.attendingPhysicianTypes.slice()
            )
          );
        // this.consultationListTypes = data.result[0].row;
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  async submit() {
    const cardExpiry = this.payment.get('cardExpiry').value;
    const ddExpiry = this.payment.get('ddDate').value;
    if (cardExpiry && ddExpiry) {
      const value = this.payment.get('cardExpiry').value;
      const valueDate = moment(value, 'DD/MM/YYYY');
      const date = moment(new Date()).format('DD/MM/YYYY');
      const dateTest = moment(date, 'DD/MM/YYYY');
      const resultDate = moment(dateTest).diff(valueDate) > 0;

      const value2 = this.payment.get('ddDate').value;
      const valueDate2 = moment(value2, 'DD/MM/YYYY');
      const date2 = moment(new Date()).format('DD/MM/YYYY');
      const dateTest2 = moment(date2, 'DD/MM/YYYY');
      const resultDate2 = moment(dateTest2).diff(valueDate2) > 0;
      if (resultDate || resultDate2) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        const company = this.corporate.get('physician').value.companyid;
        const companyid = this.corporate.get('physician').value.companyid;
        const branchid = this.corporate.get('physician').value.branchid;
        const category = this.general.get('category').value;
        const source = this.general.get('source').value;
        const referred_by_physician = this.general.get('referredBy').value
          .doctor_name;
        const gender = this.general.get('gender').value;
        const slt = this.general.get('slt').value;
        const pname = this.general.get('pname').value;
        const fathername = this.general.get('fathername').value;
        const dob = this.general.get('dob').value;
        const age = this.general.get('age').value;
        const ageType = this.general.get('ageType').value;
        const country = this.address.get('country').value.country_name;
        const state = this.address.get('state').value.state_name;
        const city = this.address.get('district').value.city_name;
        const area = this.address.get('area').value.pin_description;
        // const pincode = this.address.get('pincode').value;
        const address = this.address.get('fullAddress').value;
        const mobileNumber = this.address.get('mobileNo').value;
        // const corporateList = this.corporate.get('corporateList').value;
        const company_name = this.corporate.get('corporateList').value
          .party_name;
        const priceList = this.corporate.get('priceList').value.pricelistname;
        const partner_company = '';
        const mhc_package = this.corporate.get('package').value.packagename;
        // const mhc_packageid = this.general.get('package').value.cm_serviceid;
        const physician = this.corporate.get('physician').value.doctor_name;
        const paymentMode = this.payment.get('paymentMode').value;
        const concessionvalue = this.payment.get('consRate').value || '';
        const eft = this.payment.get('eft').value;
        const cardtype = this.payment.get('cardtype').value;
        const cardName = this.payment.get('cardName').value;
        const cardNo = this.payment.get('cardNo').value;
        const cardExpiry = this.payment.get('cardExpiry').value;
        const ddNo = this.payment.get('ddNo').value;
        const ddDate = this.payment.get('ddDate').value;
        const ddBank = this.payment.get('ddBank').value;

        console.log(
          company,
          companyid,
          branchid,
          category,
          source,
          referred_by_physician,
          gender,
          slt,
          pname,
          fathername,
          dob,
          age,
          ageType,
          country,
          state,
          city,
          area,
          address,
          mobileNumber,
          company_name,
          priceList,
          partner_company,
          mhc_package,
          // mhc_packageid,
          physician,
          paymentMode,
          concessionvalue,
          eft,
          cardtype,
          cardName,
          cardNo,
          cardExpiry,
          ddNo,
          ddDate,
          ddBank
        );
        await this.commonService
          .saveNewMHCDataAPI(
            company,
            companyid,
            branchid,
            category,
            source,
            referred_by_physician,
            gender,
            slt,
            pname,
            fathername,
            dob,
            age,
            ageType,
            country,
            state,
            city,
            area,
            address,
            mobileNumber,
            company_name,
            priceList,
            partner_company,
            mhc_package,
            // mhc_packageid,
            physician,
            paymentMode,
            concessionvalue,
            eft,
            cardtype,
            cardName,
            cardNo,
            cardExpiry,
            ddNo,
            ddDate,
            ddBank
          )
          .subscribe(
            (data: any) => {
              // console.log(data.result[0].row);
              console.log(data);
              if (data.result[0].message) {
                const message = `UHID is ${data.result[0].message[0]['UHID*']} , ${data.result[0].message[0].msg}`;
                this.snackBar.open(message, 'x', {
                  duration: 10000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.saveBth = false;
                this.saveAndPrintBtn = false;
              }
              // this.consultationListTypes = data.result[0].row;
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 5000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.message);
            }
          );
      }
    }
  }
  async submitAndContinue() {
    const cardExpiry = this.payment.get('cardExpiry').value;
    const ddExpiry = this.payment.get('ddDate').value;
    if (cardExpiry && ddExpiry) {
      const value = this.payment.get('cardExpiry').value;
      const valueDate = moment(value, 'DD/MM/YYYY');
      const date = moment(new Date()).format('DD/MM/YYYY');
      const dateTest = moment(date, 'DD/MM/YYYY');
      const resultDate = moment(dateTest).diff(valueDate) > 0;

      const value2 = this.payment.get('ddDate').value;
      const valueDate2 = moment(value2, 'DD/MM/YYYY');
      const date2 = moment(new Date()).format('DD/MM/YYYY');
      const dateTest2 = moment(date2, 'DD/MM/YYYY');
      const resultDate2 = moment(dateTest2).diff(valueDate2) > 0;
      if (resultDate || resultDate2) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        const company = this.corporate.get('physician').value.companyid;
        const companyid = this.corporate.get('physician').value.companyid;
        const branchid = this.corporate.get('physician').value.branchid;
        const category = this.general.get('category').value;
        const source = this.general.get('source').value;
        const referred_by_physician = this.general.get('referredBy').value
          .doctor_name;
        const gender = this.general.get('gender').value;
        const slt = this.general.get('slt').value;
        const pname = this.general.get('pname').value;
        const fathername = this.general.get('fathername').value;
        const dob = this.general.get('dob').value;
        const age = this.general.get('age').value;
        const ageType = this.general.get('ageType').value;
        const country = this.address.get('country').value.country_name;
        const state = this.address.get('state').value.state_name;
        const city = this.address.get('district').value.city_name;
        const area = this.address.get('area').value.pin_description;
        // const pincode = this.address.get('pincode').value;
        const address = this.address.get('fullAddress').value;
        const mobileNumber = this.address.get('mobileNo').value;
        // const corporateList = this.corporate.get('corporateList').value;
        const company_name = this.corporate.get('corporateList').value
          .party_name;
        const priceList = this.corporate.get('priceList').value.pricelistname;
        const partner_company = '';
        const mhc_package = this.corporate.get('package').value.packagename;
        // const mhc_packageid = this.general.get('package').value.cm_serviceid;
        const physician = this.corporate.get('physician').value.doctor_name;
        const paymentMode = this.payment.get('paymentMode').value;
        const concessionvalue = this.payment.get('consRate').value || '';
        const eft = this.payment.get('eft').value;
        const cardtype = this.payment.get('cardtype').value;
        const cardName = this.payment.get('cardName').value;
        const cardNo = this.payment.get('cardNo').value;
        const cardExpiry = this.payment.get('cardExpiry').value;
        const ddNo = this.payment.get('ddNo').value;
        const ddDate = this.payment.get('ddDate').value;
        const ddBank = this.payment.get('ddBank').value;

        console.log(
          company,
          companyid,
          branchid,
          category,
          source,
          referred_by_physician,
          gender,
          slt,
          pname,
          fathername,
          dob,
          age,
          ageType,
          country,
          state,
          city,
          area,
          address,
          mobileNumber,
          company_name,
          priceList,
          partner_company,
          mhc_package,
          // mhc_packageid,
          physician,
          paymentMode,
          concessionvalue,
          eft,
          cardtype,
          cardName,
          cardNo,
          cardExpiry,
          ddNo,
          ddDate,
          ddBank
        );
        await this.commonService
          .saveNewMHCDataAPI(
            company,
            companyid,
            branchid,
            category,
            source,
            referred_by_physician,
            gender,
            slt,
            pname,
            fathername,
            dob,
            age,
            ageType,
            country,
            state,
            city,
            area,
            address,
            mobileNumber,
            company_name,
            priceList,
            partner_company,
            mhc_package,
            // mhc_packageid,
            physician,
            paymentMode,
            concessionvalue,
            eft,
            cardtype,
            cardName,
            cardNo,
            cardExpiry,
            ddNo,
            ddDate,
            ddBank
          )
          .subscribe(
            (data: any) => {
              // console.log(data.result[0].row);
              console.log(data);
              if (data.result[0].message) {
                const message = `UHID is ${data.result[0].message[0]['UHID*']} , ${data.result[0].message[0].msg}`;
                this.snackBar.open(message, 'x', {
                  duration: 10000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                const UHID = data.result[0].message[0]['UHID*'].split(',');
                // console.log(UHID[0]);
                const uhidValue = UHID[0];
                const patientType = 'All';
                this.commonService
                  .getPatientListSearchDataAPI(patientType)
                  .subscribe(
                    (data: any) => {
                      // console.log(data.result[0].row);
                      // console.log(data.result[0].row);
                      const patienList = data.result[0].row;
                      const element = [];
                      patienList.map((list) => {
                        if (list.uhid === uhidValue) {
                          element.push(list);
                        }
                      });
                      // console.log(element, 'element');
                      if (element) {
                        this.router.navigate(['home/opservicebilling'], {
                          state: { data: element[0] },
                        });
                      }
                      // this.dataSource.paginator = this.paginator;
                      // this.dataSource.sort = this.sort;
                      if (data.result[0].error) {
                        this.snackBar.open(data.result[0].error.msg, 'x', {
                          duration: 3000,
                          horizontalPosition: this.horizontalPosition,
                          verticalPosition: this.verticalPosition,
                        });
                      }
                    },
                    (error) => {
                      console.log(error.message);
                    }
                  );
                this.saveBth = false;
              }
              // this.consultationListTypes = data.result[0].row;
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 5000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.message);
            }
          );
      }
    }
  }
  saveAndPrint() {
    const company = this.corporate.get('physician').value.companyid;
    const companyid = this.corporate.get('physician').value.companyid;
    const branchid = this.corporate.get('physician').value.branchid;
    const category = this.general.get('category').value;
    const source = this.general.get('source').value;
    const referred_by_physician = this.general.get('referredBy').value
      .doctor_name;
    const gender = this.general.get('gender').value;
    const slt = this.general.get('slt').value;
    const pname = this.general.get('pname').value;
    const fathername = this.general.get('fathername').value;
    const dob = this.general.get('dob').value;
    const age = this.general.get('age').value;
    const ageType = this.general.get('ageType').value;
    const country = this.address.get('country').value.country_name;
    const state = this.address.get('state').value.state_name;
    const city = this.address.get('district').value.city_name;
    const area = this.address.get('area').value.pin_description;
    // const pincode = this.address.get('pincode').value;
    const address = this.address.get('fullAddress').value;
    const mobileNumber = this.address.get('mobileNo').value;
    // const corporateList = this.corporate.get('corporateList').value;
    const company_name = this.corporate.get('corporateList').value.party_name;
    const priceList = this.corporate.get('priceList').value.pricelistname;
    const partner_company = '';
    const mhc_package = this.corporate.get('package').value.packagename;
    // const mhc_packageid = this.general.get('package').value.cm_serviceid;
    const physician = this.corporate.get('physician').value.doctor_name;
    const paymentMode = this.payment.get('paymentMode').value;
    const concessionvalue = this.payment.get('consRate').value || '';
    const eft = this.payment.get('eft').value;
    const cardtype = this.payment.get('cardtype').value;
    const cardName = this.payment.get('cardName').value;
    const cardNo = this.payment.get('cardNo').value;
    const cardExpiry = this.payment.get('cardExpiry').value;
    const ddNo = this.payment.get('ddNo').value;
    const ddDate = this.payment.get('ddDate').value;
    const ddBank = this.payment.get('ddBank').value;

    console.log(
      company,
      companyid,
      branchid,
      category,
      source,
      referred_by_physician,
      gender,
      slt,
      pname,
      fathername,
      dob,
      age,
      ageType,
      country,
      state,
      city,
      area,
      address,
      mobileNumber,
      company_name,
      priceList,
      partner_company,
      mhc_package,
      // mhc_packageid,
      physician,
      paymentMode,
      concessionvalue,
      eft,
      cardtype,
      cardName,
      cardNo,
      cardExpiry,
      ddNo,
      ddDate,
      ddBank
    );
    this.commonService
      .saveNewMHCDataAPI(
        company,
        companyid,
        branchid,
        category,
        source,
        referred_by_physician,
        gender,
        slt,
        pname,
        fathername,
        dob,
        age,
        ageType,
        country,
        state,
        city,
        area,
        address,
        mobileNumber,
        company_name,
        priceList,
        partner_company,
        mhc_package,
        // mhc_packageid,
        physician,
        paymentMode,
        concessionvalue,
        eft,
        cardtype,
        cardName,
        cardNo,
        cardExpiry,
        ddNo,
        ddDate,
        ddBank
      )
      .subscribe(
        (data: any) => {
          // console.log(data.result[0].row);
          console.log(data);
          if (data.result[0].message) {
            if (data.result[0].message[0].recordid) {
              const id = data.result[0].message[0].recordid;
              console.log(id, 'id');
              this.printId = id;
              this.printBtn = true;
              this.print();
            }
            const message = `UHID is ${data.result[0].message[0]['UHID*']} , ${data.result[0].message[0].msg}`;
            this.snackBar.open(message, 'x', {
              duration: 10000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.saveBth = false;
            this.saveAndPrintBtn = false;
          }
          // this.consultationListTypes = data.result[0].row;
          if (data.result[0].error) {
            this.snackBar.open(data.result[0].error.msg, 'x', {
              duration: 5000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        },
        (error) => {
          console.log(error.message);
        }
      );
  }
  print() {
    const id = this.printId;
    this.commonService.getPDFURL(id).subscribe(
      (data: any) => {
        console.log(data, 'data');
        console.log(data.result[0].row[0].pdffile, 'resId');
        const url =
          'https://agile-health.org/axpertwebx6/PDF/121212/121212.pdf';
        window.open(url);
      },
      (error) => {
        console.log(error.message);
      }
    );
  }
  reset() {
    this.myStepper.selectedIndex = 0;
    this.ngOnInit();
  }
}

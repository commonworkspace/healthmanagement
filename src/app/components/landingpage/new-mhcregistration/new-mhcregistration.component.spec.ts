import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMhcregistrationComponent } from './new-mhcregistration.component';

describe('NewMhcregistrationComponent', () => {
  let component: NewMhcregistrationComponent;
  let fixture: ComponentFixture<NewMhcregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMhcregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMhcregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

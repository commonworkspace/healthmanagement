import { MatStepperModule } from '@angular/material/stepper';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LandingpageComponent } from './landingpage.component';
import { LandingpageRoutes } from './landingpage.routing';
import { FlexLayoutModule } from '@angular/flex-layout';

import { DemoMaterialModule } from 'app/shared/demo.module';
import { OpregistractionComponent } from './opregistraction/opregistraction.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewMhcregistrationComponent } from './new-mhcregistration/new-mhcregistration.component';
import * as _moment from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { PatientSearchComponent } from './patient-search/patient-search.component';
import { VisitentryComponent } from './visitentry/visitentry.component';
import { MhcregistrationComponent } from './mhcregistration/mhcregistration.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { OpServiceBillingComponent } from './op-service-billing/op-service-billing.component';
import { AddServicesComponent } from './op-service-billing/add-services/add-services.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};
// export const DD_MM_YYYY_Format = {
//   parse: {
//       dateInput: 'LL',
//   },
//   display: {
//       dateInput: 'DD/MM/YYYY',
//       monthYearLabel: 'MMM YYYY',
//       dateA11yLabel: 'LL',
//       monthYearA11yLabel: 'MMMM YYYY',
//   },
// };

@NgModule({
  declarations: [
    LandingpageComponent,
    OpregistractionComponent,
    NewMhcregistrationComponent,
    PatientSearchComponent,
    VisitentryComponent,
    MhcregistrationComponent,
    PatientListComponent,
    OpServiceBillingComponent,
    AddServicesComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(LandingpageRoutes),
    DemoMaterialModule,
    FlexLayoutModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class LandingpageModule {}

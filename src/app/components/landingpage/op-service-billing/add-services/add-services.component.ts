import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { DocterName, OpBillingServices } from 'app/models/op-billing-services';
import { CommonService } from 'app/services/common.service';
import { OpservicebillingService } from 'app/services/opservicebilling.service';
import { Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';

export interface Service {
  rowno: string;
  cm_serviceid: string;
  servicename: string;
  servicetype: string;
  aqty: string;
  qty: string;
  repeat: string;
  servicetypeid: string;
  servicegroup: string;
  servicegroupid: string;
  department: string;
  departmentid: string;
  doctorinv: string;
  pers: string;
  rate_editable: string;
  qty_editable: string;
  bloodbank_flg: string;
  lis_ris_code: string;
  type_of_pack: string;
  ispackage: string;
}
@Component({
  selector: 'app-add-services',
  templateUrl: './add-services.component.html',
  styleUrls: ['./add-services.component.css'],
})
export class AddServicesComponent implements OnInit {
  public serviceForm: FormGroup;
  public serviceTypes: any = [];
  public docterBoolen: boolean = false;
  public pricelistid: string;
  public branchid: string;
  public serviceid: string;
  public docterid: string = '0';
  public companyid: string;
  public attendingPhysicianTypes: DocterName[] = [];
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  filteredServices: Observable<Service[]>;
  filteredPhysician: Observable<any[]>;
  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddServicesComponent>,
    private commonService: CommonService,
    public snackBar: MatSnackBar,
    private opBillingService?: OpservicebillingService,
    @Inject(MAT_DIALOG_DATA) public data?: any
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    console.log(this.data, 'data');
    this.serviceForm = this.fb.group({
      sno: [this.data.datas.sno],
      service: [this.data.datas.service, Validators.required],
      docterName: [this.data.datas.docterName],
      rate: [this.data.datas.rate],
      discount: [this.data.datas.discount],
      consType: [this.data.datas.consType],
      consRate: [this.data.datas.consRate],
      concession: [this.data.datas.concession],
      netAmount: [this.data.datas.netAmount],
      remarks: [this.data.datas.remarks],
    });
    this.serviceTypes = this.data.services;
    this.attendingPhysicianTypes = this.data.docters;
    this.pricelistid = this.data.pricelist;
    this.branchid = this.data.branchid;
    this.companyid = this.data.companyid;
    // console.log(this.serviceForm.controls['netAmount'].value, 'netamount');
    this.filteredServices = this.serviceForm.get('service').valueChanges.pipe(
      startWith<string | Service>(''),
      map((value) =>
        typeof value === 'string' ? value : (<any>value).servicename
      ),
      map((service) =>
        service ? this._filterservices(service) : this.serviceTypes.slice()
      )
    );
    this.disabled();
    // this.serviceForm.get('netAmount').valueChanges.subscribe((value) => {
    //   const amtvalue = this.serviceForm.get('netAmount').value;
    //   console.log(value);
    //   console.log(amtvalue, 'amtvalue');
    //   this.serviceForm.get('netAmount').setValue(amtvalue);
    // });
    this.serviceForm.get('rate').valueChanges.subscribe((value) => {
      // console.log(value);
      this.serviceForm.get('netAmount').setValue(value);
    });
    this.serviceForm.get('consRate').valueChanges.subscribe((value) => {
      // console.log(value);
      if (this.serviceForm.get('consType').value === 'Lumpsum') {
        this.serviceForm.get('consType').value;
        if (value === '') {
          const total = +this.serviceForm.get('rate').value;
          this.serviceForm.get('netAmount').setValue(total);
          this.serviceForm.get('concession').setValue(value);
        }
        if (value) {
          const consultingFee = +this.serviceForm.get('rate').value;
          const netAmount = consultingFee - value;
          this.serviceForm.get('netAmount').setValue(netAmount);
          this.serviceForm.get('concession').setValue(value);
        }
      }
      if (this.serviceForm.get('consType').value === 'Percentage') {
        if (value === '') {
          const total = +this.serviceForm.get('rate').value;
          this.serviceForm.get('netAmount').setValue(total);
          this.serviceForm.get('concession').setValue(value);
        }
        if (value) {
          const consultingFee = +this.serviceForm.get('rate').value;
          const netAmount = ((value / 100) * consultingFee).toFixed(2);
          this.serviceForm.get('netAmount').setValue(netAmount);
          this.serviceForm.get('concession').setValue(value);
        }
      }
    });
    this.serviceForm.get('docterName').valueChanges.subscribe((value) => {
      // console.log(value);
      this.docterid = value.doctor_viewid;
    });
    // console.log(this.data.datas.docterName);
    // console.log(this.serviceForm.get('rate').value, 'rateconsole');
    // console.log(this.serviceForm.get('service').value, 'service');
    if (this.serviceForm.get('service').value !== null) {
      if (this.serviceForm.get('service').value.doctorinv === 'T') {
        this.docterBoolen = true;
      } else {
        this.docterBoolen = false;
        this.serviceForm.get('docterName').setValue('');
      }
      // console.log(this.serviceForm.get('service').value, 'serviceValue');
    }
    if (this.data !== undefined) {
      console.log(this.data?.datas, 'data');
      if (this.data?.datas?.service?.rate_editable === 'No') {
        // console.log(this.data.service, 'service');
        this.serviceForm.controls['rate'].disable();
      } else {
        this.serviceForm.controls['rate'].enable();
      }
    }
    this.filteredPhysician = this.serviceForm
      .get('docterName')
      .valueChanges.pipe(
        startWith<string | any>(''),
        map((value) =>
          typeof value === 'string' ? value : (<any>value).doctor_name
        ),
        map((docterName) =>
          docterName
            ? this._filterphysician(docterName)
            : this.attendingPhysicianTypes.slice()
        )
      );
  }
  private disabled() {
    this.serviceForm.controls['netAmount'].disable();
  }
  displayFnService(service?: Service): string | undefined {
    return service ? service.servicename : undefined;
  }
  private _filterservices(value: string): Service[] {
    const filterValue = value.toLowerCase();

    return this.serviceTypes.filter((service) =>
      service.servicename.toLowerCase().includes(filterValue)
    );
  }
  displayFnphysician(docterName?: any): string | undefined {
    return docterName ? docterName.doctor_name : undefined;
  }
  private _filterphysician(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.attendingPhysicianTypes.filter((docterName) =>
      docterName.doctor_name.toLowerCase().includes(filterValue)
    );
  }
  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    console.log(event.option.value);
    this.docterid = '0';
    this.serviceid = event.option.value.cm_serviceid;
    this.serviceForm.get('consRate').setValue('');
    this.serviceForm.get('concession').setValue('');
    this.serviceForm.get('discount').setValue('');
    this.getOpServiceRate();
    // this.text = true;
    if (event.option.value.rate_editable === 'Yes') {
      // this.docterBoolen = true;
      // console.log(this.data.docters, 'docters');
      this.serviceForm.controls['rate'].enable();
    } else {
      // this.docterBoolen = false;
      // this.serviceForm.get('docterName').setValue('');
      this.serviceForm.controls['rate'].disable();
    }
    if (event.option.value.doctorinv === 'T') {
      this.docterBoolen = true;
      // console.log(this.data.docters, 'docters');
    } else {
      this.docterBoolen = false;
      this.serviceForm.get('docterName').setValue('');
    }
  }
  public async getOpServiceRate() {
    const branchid = this.branchid;
    const pricelisthdrid = this.pricelistid;
    const serviceid = this.serviceid;
    const doctorid = this.docterid;
    const companyid = this.companyid;
    await this.commonService
      .getOpServiceRateDataAPI(
        branchid,
        pricelisthdrid,
        serviceid,
        doctorid,
        companyid
      )
      .subscribe(
        (data: any) => {
          // console.log(data.result[0].row[0].oprate);
          const rate = data.result[0].row[0].oprate;
          if (rate === '') {
            this.serviceForm.get('rate').setValue(0);
          } else {
            this.serviceForm.get('rate').setValue(rate);
          }
          // console.log(this.serviceForm.get('rate').value);
          if (data.result[0].error) {
            this.snackBar.open(data.result[0].error.msg, 'x', {
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        },
        (error) => {
          console.log(error.error.message);
        }
      );
  }

  onSubmit() {
    if (
      this.serviceForm.get('service').value !== null &&
      this.serviceForm.get('service').value !== '' &&
      this.serviceForm.get('rate').value !== null &&
      this.serviceForm.get('rate').value !== ''
    ) {
      if (
        (this.docterBoolen &&
          this.serviceForm.get('docterName').value === null) ||
        (this.docterBoolen && this.serviceForm.get('docterName').value === '')
      ) {
        this.snackBar.open('Please Select the Docter Name', 'x', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        if (isNaN(this.data.datas.sno)) {
          const submitAmut = this.serviceForm.get('netAmount').value;
          // console.log(this.serviceForm.value, 'serviceForm');
          // console.log(submitAmut, 'serviceForm');
          const sno = this.serviceForm.get('sno').value;
          const service = this.serviceForm.get('service').value;
          const docterName = this.serviceForm.get('docterName').value;
          const rate = this.serviceForm.get('rate').value;
          const discount = this.serviceForm.get('discount').value;
          const consType = this.serviceForm.get('consType').value;
          const consRate = this.serviceForm.get('consRate').value;
          const concession = this.serviceForm.get('concession').value;
          const netAmount = this.serviceForm.get('netAmount').value;
          const remarks = this.serviceForm.get('remarks').value;
          const submit = {
            sno,
            service,
            docterName,
            rate,
            discount,
            consType,
            consRate,
            concession,
            netAmount,
            remarks,
          };
          this.opBillingService.addService(submit);
          this.dialogRef.close();
        } else {
          const sno = this.serviceForm.get('sno').value;
          const service = this.serviceForm.get('service').value;
          const docterName = this.serviceForm.get('docterName').value;
          const rate = this.serviceForm.get('rate').value;
          const discount = this.serviceForm.get('discount').value;
          const consType = this.serviceForm.get('consType').value;
          const consRate = this.serviceForm.get('consRate').value;
          const concession = this.serviceForm.get('concession').value;
          const netAmount = this.serviceForm.get('netAmount').value;
          const remarks = this.serviceForm.get('remarks').value;
          const submit = {
            sno,
            service,
            docterName,
            rate,
            discount,
            consType,
            consRate,
            concession,
            netAmount,
            remarks,
          };
          this.opBillingService.editService(submit);
          this.dialogRef.close();
        }
      }
    } else {
      return;
    }
  }
}

import {
  AfterViewInit,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { SessionStorageService } from 'app/auth/session-storage.service';
import {
  DocterName,
  OpBillingmodel,
  Record,
  ServiceName,
} from 'app/models/op-billing-services';
import { CommonService } from 'app/services/common.service';
import { OpservicebillingService } from 'app/services/opservicebilling.service';
import * as moment from 'moment';
import { AddServicesComponent } from './add-services/add-services.component';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

// export interface UsersData {
//   sno: number;
//   service: string;
//   docterName: string;
//   rate: string;
//   discount: string;
//   consType: string;
//   consRate: string;
//   netAmount: string;
//   remarks: string;
// }

// const ELEMENT_DATA: UsersData[] = [
//   {
//     sno: 1,
//     service: 'ACE-ANGIO TENSIN CONVERTING ENZYME SPECIAL TEST ( LABP00019 )',
//     docterName: 'Docter',
//     rate: '0',
//     discount: '0.00',
//     consType: 'any',
//     consRate: '0.00',
//     netAmount: '0.00',
//     remarks: 'test',
//   },
// ];

@Component({
  selector: 'app-op-service-billing',
  templateUrl: './op-service-billing.component.html',
  styleUrls: ['./op-service-billing.component.css'],
})
export class OpServiceBillingComponent implements OnInit {
  @ViewChild('stepper') private myStepper: MatStepper;
  isPopupOpened = true;
  saveBth: boolean = true;
  general: FormGroup;
  service: FormGroup;
  payment: FormGroup;
  public serviceTypes: any = [];
  public attendingPhysicianTypes: any = [];
  public transferOsTypes: any = [];
  public priceListTypes: any = [];
  public allServicesData: any = [];
  public concessionSourceTypes: any = [];
  public concessionReferredBy: any = [];
  public authorizedBy: any = [];
  public newObject: Object = {};
  public newObject2: Object = {};
  public newObject3: Object = {};
  public newObject4: Object = {};
  public newObject5: Object = {};
  public newObject6: Object = {};
  public newObject7: Object = {};
  public newObject8: Object = {};
  public newObject9: Object = {};
  public newObject10: Object = {};
  public newObjectTest: any = [];
  public bankTypes: any = [];
  filteredReferredBy: Observable<any[]>;
  // displayedColumns: string[] = [
  //   'S.No',
  //   'Service',
  //   'Docter Name',
  //   'Rate',
  //   'L.Discount',
  //   'Cons.Type',
  //   'Cons.Rate',
  //   'Net Amount',
  //   'Remarks',
  //   'Edit',
  //   'Delete',
  // ];
  // dataSource = ELEMENT_DATA;
  // @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  public paymentModeTypes: any = [];
  public ddexpiredMinDate = new Date();
  public ddexpiredMaxDate = new Date(2500, 1, 1);

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private injector: Injector,
    private router: Router,
    private sessionService: SessionStorageService,
    private fb: FormBuilder,
    private commonService: CommonService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private opBillingService?: OpservicebillingService
  ) {}

  ngOnInit(): void {
    this.sessionService.tokenValidation();
    if (history.state.data === undefined) {
      const routerService = this.injector.get(Router);
      routerService.navigateByUrl('/home/patientList', {
        skipLocationChange: false,
      });
    } else {
      console.log(history.state.data, 'mhc');
      this.general = this.fb.group({
        billno: ['Auto'],
        uhid: [''],
        opno: [''],
        pname: [''],
        physician: [''],
        billType: ['Cash'],
        priceList: ['cash'],
        mobileNo: [''],
        concessionSource: [''],
        concessionReferredBy: [''],
        authorized: [''],
      });
      this.service = this.fb.group({
        corporateList: [''],
        priceList: [''],
      });
      this.payment = this.fb.group({
        concessionAmount: 0,
        netAmount: [''],
        paymentMode: ['Cash', Validators.required],
        toPayBy: [''],
        transferOs: [''],
        remark: [''],
        topay: [''],
        eft: [''],
        cardtype: [''],
        cardName: [''],
        cardNo: [''],
        cardExpiry: [''],
        ddNo: [''],
        ddDate: [''],
        ddBank: [''],
      });
      if (history.state.data.corporateid === '') {
        this.getMasterData();
        // this.getDocterMasterList();
      } else {
        this.payment.get('toPayBy').setValue('Corporate');
        this.getMasterDataCorporate();
        setTimeout(() => {
          this.getCorporateList();
        }, 1000);
        this.payment.get('transferOs').setValue(history.state.data.corporate);
      }
      this.getPriceListApi();
      // setTimeout(() => {
      //   this.getDocterMasterList();
      // }, 500);
      this.general.get('pname').setValue(history.state.data.dis_name);
      this.general.get('uhid').setValue(history.state.data.uhid);
      this.general.get('opno').setValue(history.state.data.opn);
      this.general.get('physician').setValue(history.state.data.doctor_name);
      this.general.get('mobileNo').setValue(history.state.data.mobile_primary);
      this.disabled();

      this.ServiceList.length = 0;
      // console.log(this.opBillingService.opBillingServiceList, 'servicelist');
      this.general.get('concessionSource').valueChanges.subscribe((value) => {
        this.general.get('concessionReferredBy').setValue('');
        this.general.get('authorized').setValue('');
        if (value === '') {
          this.general.controls['concessionReferredBy'].disable();
          this.general.controls['authorized'].disable();
          this.general.get('concessionReferredBy').setValue('');
          this.general.get('authorized').setValue('');
        } else {
          if (value.mastervalue === 'Doctor') {
            this.getReferredDocterApi(value.mastervalue);
            this.general.controls['concessionReferredBy'].enable();
            this.general.controls['authorized'].enable();
          }
          if (value.mastervalue === 'Chairman') {
            this.getReferredDocterApi(value.mastervalue);
            this.general.controls['concessionReferredBy'].enable();
            this.general.controls['authorized'].enable();
          }
          if (value.mastervalue === 'Insurance') {
            this.getReferredDocterApi(value.mastervalue);
            this.general.controls['concessionReferredBy'].enable();
            this.general.controls['authorized'].enable();
          }
        }
      });
      // if(this.general.get('billType').value === 'Credit'){
      //   this.paymentModeTypes = [
      //     {
      //       rowno: '12',
      //       mastertype: 'Payment Mode',
      //       masterid: '10116000000050',
      //       mastervalue: 'Credit',
      //     },
      //   ];
      //   setTimeout(() => {
      //     // console.log(this.payment.get('paymentMode').value, 'Payment');
      //     this.payment
      //       .get('paymentMode')
      //       .setValue(this.paymentModeTypes[0].mastervalue);
      //   }, 300);
      // }else{
      //   this.commonService.getMasterDataAPI().subscribe(
      //     (data: any) => {
      //       // console.log(data.result[0].row);
      //       const datas = data.result[0].row;
      //       // this.countryTypes = data.result[0].row;
      //       var groupBy = function (xs, key) {
      //         return xs.reduce(function (rv, x) {
      //           (rv[x[key]] = rv[x[key]] || []).push(x);
      //           return rv;
      //         }, {});
      //       };
      //       var groubedByTeam = groupBy(datas, 'mastertype');
      //       // this.patientCategoryTypes = groubedByTeam['Patient Category'];
      //       // this.sourceTypes = groubedByTeam['Source'];
      //       // this.paymentModeTypes = groubedByTeam['Payment Mode'];
      //       // this.bankTypes = groubedByTeam['Bank'];
      //       let paymentModeSort = groubedByTeam['Payment Mode'];
      //       this.paymentModeTypes = [];
      //       paymentModeSort.map((value) => {
      //         // console.log(value);
      //         if (value.mastervalue !== 'Credit') {
      //           // this.paymentModeTypes = value;

      //           this.paymentModeTypes.push(value);
      //         }
      //       });
      //       setTimeout(() => {
      //         // console.log(this.payment.get('paymentMode').value, 'Payment');
      //         this.payment
      //           .get('paymentMode')
      //           .setValue(this.paymentModeTypes[1].mastervalue);
      //       }, 300);
      //       // console.log(groubedByTeam);
      //       if (data.result[0].error) {
      //         this.snackBar.open(data.result[0].error.msg, 'x', {
      //           duration: 3000,
      //           horizontalPosition: this.horizontalPosition,
      //           verticalPosition: this.verticalPosition,
      //         });
      //       }
      //     },
      //     (error) => {
      //       console.log(error.error.message);
      //     }
      //   );
      //   this.payment.get('toPayBy').setValue('');
      //   this.payment.get('transferOs').setValue('');
      //   this.payment.get('remark').setValue('');
      //   this.payment.get('topay').setValue('');
      // }
      this.general.get('billType').valueChanges.subscribe((value) => {
        if (value === 'Credit') {
          this.paymentModeTypes = [
            {
              rowno: '12',
              mastertype: 'Payment Mode',
              masterid: '10116000000050',
              mastervalue: 'Credit',
            },
          ];
          setTimeout(() => {
            // console.log(this.payment.get('paymentMode').value, 'Payment');
            this.payment
              .get('paymentMode')
              .setValue(this.paymentModeTypes[0].mastervalue);
          }, 300);
        }
        if (value === 'Cash') {
          this.commonService.getMasterDataAPI().subscribe(
            (data: any) => {
              // console.log(data.result[0].row);
              const datas = data.result[0].row;
              // this.countryTypes = data.result[0].row;
              var groupBy = function (xs, key) {
                return xs.reduce(function (rv, x) {
                  (rv[x[key]] = rv[x[key]] || []).push(x);
                  return rv;
                }, {});
              };
              var groubedByTeam = groupBy(datas, 'mastertype');
              // this.patientCategoryTypes = groubedByTeam['Patient Category'];
              // this.sourceTypes = groubedByTeam['Source'];
              // this.paymentModeTypes = groubedByTeam['Payment Mode'];
              // this.bankTypes = groubedByTeam['Bank'];
              let paymentModeSort = groubedByTeam['Payment Mode'];
              this.paymentModeTypes = [];
              paymentModeSort.map((value) => {
                // console.log(value);
                if (value.mastervalue !== 'Credit') {
                  // this.paymentModeTypes = value;

                  this.paymentModeTypes.push(value);
                }
              });
              setTimeout(() => {
                // console.log(this.payment.get('paymentMode').value, 'Payment');
                this.payment
                  .get('paymentMode')
                  .setValue(this.paymentModeTypes[1].mastervalue);
              }, 300);
              // console.log(groubedByTeam);
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.error.message);
            }
          );
          this.payment.get('toPayBy').setValue('');
          this.payment.get('transferOs').setValue('');
          this.payment.get('remark').setValue('');
          this.payment.get('topay').setValue('');
        }
      });
      this.payment.get('toPayBy').valueChanges.subscribe((value) => {
        if (value === 'Doctor') {
          // this.payment.get('paymentMode').setValue(value);
          this.getTransferOSDocterApi(value);
        }
        if (value === 'Corporate') {
          // this.payment.get('paymentMode').setValue(value);
          this.getCorporateList();
        }
        if (value === 'Patient') {
          // this.payment.get('paymentMode').setValue(value);
          this.transferOsTypes = [];
        }
        if (value === 'Employee') {
          // this.payment.get('paymentMode').setValue(value);
          this.transferOsTypes = [];
        }
      });
      this.payment.get('paymentMode').valueChanges.subscribe((value) => {
        // console.log(value);
        if (value) {
          const date = moment(new Date()).format('DD/MM/YYYY');
          this.payment.get('eft').setValue('');
          this.payment.get('cardtype').setValue('');
          this.payment.get('cardName').setValue('');
          this.payment.get('cardNo').setValue('');
          this.payment.get('cardExpiry').setValue(date);
          this.payment.get('ddNo').setValue('');
          this.payment.get('ddDate').setValue(date);
          this.payment.get('ddBank').setValue('');
        }
      });
      const date = moment(new Date()).format('DD/MM/YYYY');
      this.payment.get('cardExpiry').setValue(date);
      this.payment.get('ddDate').setValue(date);
      this.payment.get('ddDate').valueChanges.subscribe((value) => {
        // console.log(value);

        const valueDate = moment(value, 'DD/MM/YYYY');
        // const valueDate = moment(value).format('DD/MM/YYYY');
        // console.log(valueDate, 'valueDate');
        const date = moment(new Date()).format('DD/MM/YYYY');
        const date2 = moment(date, 'DD/MM/YYYY');
        // console.log(date, 'date');
        const resultDate = moment(date2).diff(valueDate) > 0;
        if (resultDate) {
          this.snackBar.open('Not a Valid Date', 'x', {
            duration: 5000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
        console.log(resultDate, 'resultDate');
      });
      this.saveBth = true;

      // this.filteredReferredBy = this.general
      //   .get('concessionReferredBy')
      //   .valueChanges.pipe(
      //     startWith<string | any>(''),
      //     map((value) =>
      //       typeof value === 'string' ? value : (<any>value).approver_name
      //     ),

      //     map((referredBy) =>
      //       referredBy
      //         ? this._filterReferredBy(referredBy)
      //         : this.concessionReferredBy.slice()
      //     )
      //   );
    }
  }
  onStepChange(e) {
    console.log(e);
    console.log(this.attendingPhysicianTypes, 'this.attendingPhysicianTypes');
    if (e.selectedIndex === 1) {
      if (this.attendingPhysicianTypes.length === 0) {
        this.getDocterMasterList();
      }
      if (this.serviceTypes.length === 0) {
        this.getOpServiceData(history.state.data.branchid);
      }
    }
  }
  // datachange() {
  //   console.log(this.concessionReferredBy, 'concessionReferredBy');
  // }
  // ngAfterViewInit() {
  //   console.log(this.concessionReferredBy, 'concessionReferredBy');
  // }
  displayFnreferredBy(refDocter?: any): string | undefined {
    console.log(refDocter);
    return refDocter ? refDocter.approver_name : undefined;
  }
  private _filterReferredBy(value: string): any[] {
    console.log(value, 'filter');
    console.log(this.concessionReferredBy, 'this.concessionReferredBy');
    const filterValue = value.toLowerCase();
    return this.concessionReferredBy.filter((refDocter) =>
      refDocter.approver_name.toLowerCase().includes(filterValue)
    );
  }

  public getPriceListApi() {
    if (history.state.data.corporateid === '') {
      const e = '0';
      // console.log(e);
      this.getPriceList(e);
    } else {
      const e = history.state.data.corporateid;
      // this.paymentModeTypes = [];
      console.log(e);
      this.getPriceList(e);
      this.general.get('billType').setValue('Credit');
      this.paymentModeTypes = [
        {
          rowno: '12',
          mastertype: 'Payment Mode',
          masterid: '10116000000050',
          mastervalue: 'Credit',
        },
      ];
      setTimeout(() => {
        // console.log(this.payment.get('paymentMode').value, 'Payment');
        this.payment
          .get('paymentMode')
          .setValue(this.paymentModeTypes[0].mastervalue);
      }, 300);
      // this.priceListTypesCorporate();
    }
  }
  public async getPriceList(e) {
    const corporateid = e;
    console.log(e, 'corporateList');
    await this.commonService.getPriceListDataAPI(corporateid).subscribe(
      (data: any) => {
        // this.getDocterMasterList();
        // console.log(data.result[0].row);
        this.priceListTypes = data.result[0].row;
        setTimeout(() => {
          this.general.get('priceList').setValue(this.priceListTypes[0]);
          console.log(this.general.get('priceList').value, 'PriceList');
          // if(this.general.get('priceList').value.pricelistname !== "General"){

          // }
        }, 300);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getReferredDocterApi(type) {
    await this.commonService.getReferredDocterDataAPI().subscribe(
      (data: any) => {
        console.log(data.result[0].row);
        const datas = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'approver_type');
        this.authorizedBy = groubedByTeam[''];
        if (type === 'Doctor') {
          // setTimeout(() => {
          this.concessionReferredBy = groubedByTeam['Doctor'];
          this.filteredReferredBy = this.general
            .get('concessionReferredBy')
            .valueChanges.pipe(
              startWith<string | any>(''),
              map((value) =>
                typeof value === 'string' ? value : (<any>value).approver_name
              ),

              map((referredBy) =>
                referredBy
                  ? this._filterReferredBy(referredBy)
                  : this.concessionReferredBy.slice()
              )
            );

          // console.log(this.concessionReferredBy, 'API.concessionReferredBy ');
          // const result = this.concessionReferredBy;
          // this.concessionReferredBy.push(result);
          // this.general.get('concessionReferredBy').setValue(result);
          // console.log(
          //   this.general.get('concessionReferredBy').value,
          //   'oninit.concessionReferredBy '
          // );
          // this.filteredReferredBy = this.general
          //   .get('concessionReferredBy')
          //   .valueChanges.pipe(
          //     startWith<string | any>(''),
          //     map((value) =>
          //       typeof value === 'string' ? value : (<any>value).approver_name
          //     ),

          //     map((referredBy) =>
          //       referredBy
          //         ? this._filterReferredBy(referredBy)
          //         : this.concessionReferredBy.slice()
          //     )
          //   );
          // this.filteredReferredBy = groubedByTeam['Doctor'];
          // }, 200);
        }
        if (type === 'Chairman') {
          // setTimeout(() => {
          this.concessionReferredBy = groubedByTeam['Chairman'];
          this.filteredReferredBy = this.general
            .get('concessionReferredBy')
            .valueChanges.pipe(
              startWith<string | any>(''),
              map((value) =>
                typeof value === 'string' ? value : (<any>value).approver_name
              ),

              map((referredBy) =>
                referredBy
                  ? this._filterReferredBy(referredBy)
                  : this.concessionReferredBy.slice()
              )
            );
          // this.filteredReferredBy = groubedByTeam['Chairman'];
          // console.log(this.concessionReferredBy, 'API.concessionReferredBy ');
          // const result = this.concessionReferredBy;
          // this.general.get('concessionReferredBy').setValue(result);
          // console.log(
          //   this.general.get('concessionReferredBy').value,
          //   'oninit.concessionReferredBy '
          // );
          // }, 200);
        }
        if (type === 'Insurance') {
          // setTimeout(() => {
          this.concessionReferredBy = groubedByTeam['Insurance'];
          this.filteredReferredBy = this.general
            .get('concessionReferredBy')
            .valueChanges.pipe(
              startWith<string | any>(''),
              map((value) =>
                typeof value === 'string' ? value : (<any>value).approver_name
              ),

              map((referredBy) =>
                referredBy
                  ? this._filterReferredBy(referredBy)
                  : this.concessionReferredBy.slice()
              )
            );
          // this.filteredReferredBy = groubedByTeam['Insurance'];
          // console.log(this.concessionReferredBy, 'API.concessionReferredBy ');
          // const result = this.concessionReferredBy;
          // this.general.get('concessionReferredBy').setValue(result);
          // console.log(
          //   this.general.get('concessionReferredBy').value,
          //   'oninit.concessionReferredBy '
          // );
          // }, 200);
        }
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }

  public async getOpServiceData(branchid) {
    await this.commonService.getOpServiceDataAPI(branchid).subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.serviceTypes = data.result[0].row;
        // const datas = data.result[0].row;
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getDocterMasterList() {
    await this.commonService.getDocterMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row, 'docterdata');
        // const datas = data.result[0].row;
        this.attendingPhysicianTypes = data.result[0].row;
        // var groupBy = function (xs, key) {
        //   return xs.reduce(function (rv, x) {
        //     (rv[x[key]] = rv[x[key]] || []).push(x);
        //     return rv;
        //   }, {});
        // };
        // var groubedByTeam = groupBy(datas, 'dtype');
        // console.log(groubedByTeam);
        // this.attendingPhysicianTypes = groubedByTeam['In House'];
        // this.consultationListTypes = data.result[0].row;
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getTransferOSDocterApi(type) {
    await this.commonService.getDocterMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        // const datas = data.result[0].row;
        // var groupBy = function (xs, key) {
        //   return xs.reduce(function (rv, x) {
        //     (rv[x[key]] = rv[x[key]] || []).push(x);
        //     return rv;
        //   }, {});
        // };
        // var groubedByTeam = groupBy(datas, 'approver_type');
        if (type === 'Doctor') {
          this.transferOsTypes = data.result[0].row;
        }
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }

  public async getCorporateList() {
    await this.commonService.getCorporateListDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        this.transferOsTypes = data.result[0].row;
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public disabled() {
    this.general.controls['concessionReferredBy'].disable();
    this.general.controls['authorized'].disable();
    this.general.controls['billno'].disable();
    this.general.controls['pname'].disable();
    this.general.controls['uhid'].disable();
    this.general.controls['opno'].disable();
    this.general.controls['physician'].disable();
    this.general.controls['mobileNo'].disable();
    this.general.controls['priceList'].disable();
    this.payment.controls['concessionAmount'].disable();
    this.payment.controls['netAmount'].disable();
    // this.opEpisode.controls['consultingFee'].disable();
    // this.payment.controls['packageFee'].disable();
    // this.payment.controls['totalAmount'].disable();
  }

  date2(e) {
    console.log(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    // var value = datePipe.transform(e.value._d, 'dd-MM-yyyy');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.payment.get('cardExpiry').setValue(dateValue);
    // console.log(value);
  }
  date3(e) {
    console.log(e.value);
    // var convertDate = new Date(e.value).toISOString().substring(0, 10);
    var datePipe = new DatePipe('en-US');
    // var value = datePipe.transform(e.value._d, 'dd-MM-yyyy');
    var value = datePipe.transform(e.value, 'MM-dd-yyyy');
    var dateValue = datePipe.transform(e.value, 'dd/MM/yyyy');
    this.payment.get('ddDate').setValue(dateValue);
    // console.log(value);
  }

  get ServiceList() {
    this.allServicesData = this.opBillingService.getAllService();
    if (this.allServicesData.length > 0) {
      console.log(this.allServicesData, 'allServicesData');
      const netAmountArray = [];
      const consAmountArray = [];
      this.allServicesData.map((listNetAmount, idx) => {
        console.log(idx, 'idx');
        netAmountArray.push(+listNetAmount.netAmount);
        consAmountArray.push(+listNetAmount.consRate);
        // let add = +listNetAmount.netAmount + +listNetAmount.netAmount;
      });
      console.log(netAmountArray);
      if (netAmountArray.length > 0) {
        let sum = netAmountArray.reduce(function (a, b) {
          return a + b;
        }, 0);

        console.log(sum);
        this.payment.get('netAmount').setValue(sum);
        this.payment.get('topay').setValue(sum);
      }
      if (consAmountArray.length > 0) {
        let sum = consAmountArray.reduce(function (a, b) {
          return a + b;
        }, 0);

        console.log(sum);
        this.payment.get('concessionAmount').setValue(sum);
      }
    }
    return this.opBillingService.getAllService();
  }

  addService() {
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(AddServicesComponent, {
      data: {
        branchid: history.state.data.branchid,
        companyid: history.state.data.companyid,
        datas: {},
        services: this.serviceTypes,
        docters: this.attendingPhysicianTypes,
        pricelist: this.general.get('priceList').value.pricelisthdrid,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.isPopupOpened = false;
    });
  }

  editService(sno: number) {
    this.isPopupOpened = true;
    const service = this.opBillingService
      .getAllService()
      .find((c) => c.sno === sno);
    const dialogRef = this.dialog.open(AddServicesComponent, {
      data: {
        branchid: history.state.data.branchid,
        companyid: history.state.data.companyid,
        datas: service,
        services: this.serviceTypes,
        docters: this.attendingPhysicianTypes,
        pricelist: this.general.get('priceList').value.pricelisthdrid,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.isPopupOpened = false;
    });
  }

  deleteService(sno: number) {
    this.opBillingService.deleteService(sno);
  }

  public async getMasterDataCorporate() {
    await this.commonService.getMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        const datas = data.result[0].row;
        // this.countryTypes = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'mastertype');
        // this.patientCategoryTypes = groubedByTeam['Patient Category'];
        this.concessionSourceTypes = groubedByTeam['Concession Source'];
        // let paymentModeSort = groubedByTeam['Payment Mode'];
        // paymentModeSort.map((value) => {
        //   // console.log(value);
        //   if (value.mastervalue !== 'Credit') {
        //     // this.paymentModeTypes = value;
        //     this.paymentModeTypes.push(value);
        //   }
        // });
        this.bankTypes = groubedByTeam['Bank'];
        // console.log(groubedByTeam);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  public async getMasterData() {
    await this.commonService.getMasterDataAPI().subscribe(
      (data: any) => {
        // console.log(data.result[0].row);
        const datas = data.result[0].row;
        // this.countryTypes = data.result[0].row;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };
        var groubedByTeam = groupBy(datas, 'mastertype');
        // this.patientCategoryTypes = groubedByTeam['Patient Category'];
        this.concessionSourceTypes = groubedByTeam['Concession Source'];
        let paymentModeSort = groubedByTeam['Payment Mode'];
        paymentModeSort.map((value) => {
          // console.log(value);
          if (value.mastervalue !== 'Credit') {
            // this.paymentModeTypes = value;
            this.paymentModeTypes.push(value);
          }
        });
        this.bankTypes = groubedByTeam['Bank'];
        // console.log(groubedByTeam);
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }
  goBack(stepper: MatStepper) {
    stepper.previous();
  }

  goForward(stepper: MatStepper) {
    stepper.next();
  }

  // openDialog(action,obj) {
  //   obj.action = action;
  //   const dialogRef = this.dialog.open(ServiceTableComponent, {
  //     width: '250px',
  //     data:obj
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if(result.event == 'Add'){
  //       this.addRowData(result.data);
  //     }else if(result.event == 'Update'){
  //       this.updateRowData(result.data);
  //     }else if(result.event == 'Delete'){
  //       this.deleteRowData(result.data);
  //     }
  //   });
  // }

  // addRowData(row_obj){
  //   var d = new Date();
  //   this.dataSource.push({
  //     id:d.getTime(),
  //     name:row_obj.name
  //   });
  //   this.table.renderRows();

  // }
  // updateRowData(row_obj){
  //   this.dataSource = this.dataSource.filter((value,key)=>{
  //     if(value.id == row_obj.id){
  //       value.name = row_obj.name;
  //     }
  //     return true;
  //   });
  // }
  // deleteRowData(row_obj){
  //   this.dataSource = this.dataSource.filter((value,key)=>{
  //     return value.id != row_obj.id;
  //   });
  // }
  async submit() {
    const ddExpiry = this.payment.get('ddDate').value;
    if (ddExpiry) {
      // const value = this.payment.get('cardExpiry').value;
      // const valueDate = moment(value, 'DD/MM/YYYY');
      // const date = moment(new Date()).format('DD/MM/YYYY');
      // const resultDate = moment(date).diff(valueDate) >= 0;

      const value2 = this.payment.get('ddDate').value;
      const valueDate2 = moment(value2, 'DD/MM/YYYY');
      const date2 = moment(new Date()).format('DD/MM/YYYY');
      const dateTest = moment(date2, 'DD/MM/YYYY');
      const resultDate2 = moment(dateTest).diff(valueDate2) > 0;
      if (resultDate2) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        // console.log(this.allServicesData, 'submit');
        const paymentMode = this.payment.get('paymentMode').value;
        const billtype = this.general.get('billType').value;
        const concessionSource = this.general.get('concessionSource').value
          .mastervalue;
        const concessionReferredBy = this.general.get('concessionReferredBy')
          .value.approver_name;
        const authorized = this.general.get('authorized').value;
        const topayby = this.payment.get('toPayBy').value;
        const ostransfer = this.payment.get('transferOs').value;
        const remark = this.payment.get('remark').value;
        const eft = this.payment.get('eft').value;
        const cardtype = this.payment.get('cardtype').value;
        const cardName = this.payment.get('cardName').value;
        const cardNo = this.payment.get('cardNo').value;
        const ddNo = this.payment.get('ddNo').value;
        const ddDate = this.payment.get('ddDate').value;
        const ddBank = this.payment.get('ddBank').value;
        this.allServicesData.map((value, idx) => {
          const concession = value.concession === '' ? '0' : value.concession;
          const concessionType = value.consType === null ? '' : value.consType;
          const concessionValue = value.consRate === '' ? '0' : value.consRate;
          const doctorname =
            value.service.doctorinv === 'T'
              ? '0'
              : history.state.data.attending_physician;
          const hidden_payamount = `-${value.netAmount}`;

          const sdocterName = new DocterName(
            value.docterName.doctor_name || '',
            value.docterName.doctor_viewid || '',
            '',
            ''
          );
          const serviceName = new ServiceName(
            value.service.servicename,
            value.service.cm_serviceid,
            '',
            ''
          );
          const opBillingService = new OpBillingmodel(
            value.service.servicename,
            serviceName,
            value.service.cm_serviceid,
            value.service.ispackage,
            value.service.type_of_pack,
            value.service.lis_ris_code,
            value.service.servicegroup,
            value.service.servicegroupid,
            value.service.servicetype,
            value.service.servicetypeid,
            value.service.department,
            value.service.departmentid,
            doctorname,
            value.service.doctorinv,
            sdocterName,
            value.docterName.doctor_viewid || '',
            value.service.qty_editable,
            value.service.rate_editable,
            value.service.qty,
            value.rate,
            value.netAmount,
            concessionType,
            concessionValue,
            value.netAmount,
            concession,
            value.netAmount,
            hidden_payamount,
            '0',
            '0',
            ''
          );
          var str = '' + (idx + 1);
          var pad = '000';
          str = pad.substring(0, pad.length - str.length) + str;
          const billingScreen = new Record(str, '0', opBillingService);
          this.newObjectTest.push(billingScreen);
        });
        console.log(JSON.stringify(this.newObjectTest), 'this.newObjectTest ');
        const arr = JSON.parse(JSON.stringify(this.newObjectTest));
        await this.commonService
          .billingValueObject(
            arr,
            billtype,
            paymentMode,
            concessionSource,
            concessionReferredBy,
            authorized,
            topayby,
            ostransfer,
            remark,
            eft,
            cardtype,
            cardName,
            cardNo,
            ddNo,
            ddDate,
            ddBank
          )
          .subscribe(
            (data: any) => {
              console.log(data);
              if (data.result[0].message) {
                const message = `Bill No. is ${data.result[0].message[0]['Bill No.*']} , ${data.result[0].message[0].msg}`;
                this.snackBar.open(message, 'x', {
                  duration: 10000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.saveBth = false;
              }
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 5000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.error.message);
            }
          );
      }
    }
  }
  async saveAndContinue() {
    const ddExpiry = this.payment.get('ddDate').value;
    if (ddExpiry) {
      // const value = this.payment.get('cardExpiry').value;
      // const valueDate = moment(value, 'DD/MM/YYYY');
      // const date = moment(new Date()).format('DD/MM/YYYY');
      // const resultDate = moment(date).diff(valueDate) >= 0;

      const value2 = this.payment.get('ddDate').value;
      const valueDate2 = moment(value2, 'DD/MM/YYYY');
      const date2 = moment(new Date()).format('DD/MM/YYYY');
      const dateTest = moment(date2, 'DD/MM/YYYY');
      const resultDate2 = moment(dateTest).diff(valueDate2) > 0;
      if (resultDate2) {
        this.snackBar.open('Not a Valid Date', 'x', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        // console.log(this.allServicesData, 'submit');
        const paymentMode = this.payment.get('paymentMode').value;
        const billtype = this.general.get('billType').value;
        // const concessionSource = this.general.get('concessionSource').value.mastervalue;
        const concessionSource = this.general.get('concessionSource').value
          .mastervalue;
        const concessionReferredBy = this.general.get('concessionReferredBy')
          .value.approver_name;
        const authorized = this.general.get('authorized').value;
        const topayby = this.payment.get('toPayBy').value;
        const ostransfer = this.payment.get('transferOs').value;
        const remark = this.payment.get('remark').value;
        const eft = this.payment.get('eft').value;
        const cardtype = this.payment.get('cardtype').value;
        const cardName = this.payment.get('cardName').value;
        const cardNo = this.payment.get('cardNo').value;
        const ddNo = this.payment.get('ddNo').value;
        const ddDate = this.payment.get('ddDate').value;
        const ddBank = this.payment.get('ddBank').value;
        this.allServicesData.map((value, idx) => {
          const concession = value.concession === '' ? '0' : value.concession;
          const concessionType = value.consType === null ? '' : value.consType;
          const concessionValue = value.consRate === '' ? '0' : value.consRate;
          const doctorname =
            value.service.doctorinv === 'T'
              ? '0'
              : history.state.data.attending_physician;
          const hidden_payamount = `-${value.netAmount}`;

          const sdocterName = new DocterName(
            value.docterName.doctor_name || '',
            value.docterName.doctor_viewid || '',
            '',
            ''
          );
          const serviceName = new ServiceName(
            value.service.servicename,
            value.service.cm_serviceid,
            '',
            ''
          );
          const opBillingService = new OpBillingmodel(
            value.service.servicename,
            serviceName,
            value.service.cm_serviceid,
            value.service.ispackage,
            value.service.type_of_pack,
            value.service.lis_ris_code,
            value.service.servicegroup,
            value.service.servicegroupid,
            value.service.servicetype,
            value.service.servicetypeid,
            value.service.department,
            value.service.departmentid,
            doctorname,
            value.service.doctorinv,
            sdocterName,
            value.docterName.doctor_viewid || '',
            value.service.qty_editable,
            value.service.rate_editable,
            value.service.qty,
            value.rate,
            value.netAmount,
            concessionType,
            concessionValue,
            value.netAmount,
            concession,
            value.netAmount,
            hidden_payamount,
            '0',
            '0',
            ''
          );
          var str = '' + (idx + 1);
          var pad = '000';
          str = pad.substring(0, pad.length - str.length) + str;
          const billingScreen = new Record(str, '0', opBillingService);
          this.newObjectTest.push(billingScreen);
        });
        console.log(JSON.stringify(this.newObjectTest), 'this.newObjectTest ');
        const arr = JSON.parse(JSON.stringify(this.newObjectTest));
        await this.commonService
          .billingValueObject(
            arr,
            billtype,
            paymentMode,
            concessionSource,
            concessionReferredBy,
            authorized,
            topayby,
            ostransfer,
            remark,
            eft,
            cardtype,
            cardName,
            cardNo,
            ddNo,
            ddDate,
            ddBank
          )
          .subscribe(
            (data: any) => {
              // console.log(data);
              if (data.result[0].message) {
                const message = `Bill No. is ${data.result[0].message[0]['Bill No.*']} , ${data.result[0].message[0].msg}`;
                this.snackBar.open(message, 'x', {
                  duration: 10000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.router.navigate(['home/patientList']);
              }
              if (data.result[0].error) {
                this.snackBar.open(data.result[0].error.msg, 'x', {
                  duration: 5000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            },
            (error) => {
              console.log(error.error.message);
            }
          );
      }
    }
  }
  reset() {
    this.myStepper.selectedIndex = 0;
    this.ngOnInit();
  }
}

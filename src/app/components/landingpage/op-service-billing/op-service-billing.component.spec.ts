import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpServiceBillingComponent } from './op-service-billing.component';

describe('OpServiceBillingComponent', () => {
  let component: OpServiceBillingComponent;
  let fixture: ComponentFixture<OpServiceBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpServiceBillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpServiceBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

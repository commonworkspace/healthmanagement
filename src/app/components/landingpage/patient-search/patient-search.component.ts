import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SessionStorageService } from 'app/auth/session-storage.service';
import { MatAccordion } from '@angular/material/expansion';
import { CommonService } from 'app/services/common.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
export interface SearchElement {
  a: string;
  address: string;
  age: string;
  agetype: string;
  b: string;
  c: string;
  city: string;
  column1: string;
  eradm: string;
  lcdiscount: string;
  mobile_primary: string;
  patient_name: string;
  rowno: string;
  uhid: string;
}
// const searchList: SearchElement[] = [];
@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.css'],
})
export class PatientSearchComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  displayedColumns: string[] = [
    'UHID',
    'Patient Name',
    'Age',
    'Age Type',
    'Mobile No',
    'Address',
    'OP',
    'MHC',
  ];
  search: FormGroup;
  public searchList: SearchElement[] = [];
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  dataSource: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  // dataSource = new MatTableDataSource<[]>(this.searchList);
  constructor(
    private fb: FormBuilder,
    private sessionService: SessionStorageService,
    private commonService: CommonService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sessionService.tokenValidation();
    this.search = this.fb.group({
      branch: ['Aminjikarai'],
      uhid: '',
      pname: '',
      spousename: '',
      fname: '',
      gender: 'ALL',
      mobileNo: ['', Validators.compose([Validators.pattern('[6-9]\\d{9}')])],
      email: '',
      oldNo: '',
    });
    // this.searchCall();
  }
  _keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };
  // ngAfterViewInit() {
  //   // this.dataSource.paginator = this.paginator;
  //   this.dataSource = this.searchList;
  //   this.dataSource.sort = this.sort;
  // }
  public async searchCall() {
    const branch = this.search.get('branch').value;
    const uhid = this.search.get('uhid').value;
    const pname = this.search.get('pname').value;
    const spousename = this.search.get('spousename').value;
    const fname = this.search.get('fname').value;
    const gender = this.search.get('gender').value;
    const mobile = this.search.get('mobileNo').value;
    const email = this.search.get('email').value;
    const oldNo = this.search.get('oldNo').value;
    await this.commonService
      .getSearchDataAPI(
        branch,
        uhid,
        pname,
        spousename,
        fname,
        gender,
        mobile,
        email,
        oldNo
      )
      .subscribe(
        (data: any) => {
          // console.log(data.result[0].row);
          // console.log(data);
          this.accordion.closeAll();
          this.searchList = data.result[0].row;
          this.dataSource = new MatTableDataSource<SearchElement>(
            this.searchList
          );
          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          });
          // this.dataSource.paginator = this.paginator;
          // this.dataSource.sort = this.sort;
          if (data.result[0].error) {
            this.snackBar.open(data.result[0].error.msg, 'x', {
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        },
        (error) => {
          console.log(error.message);
        }
      );
  }
  public reset() {
    this.search.reset();
  }
  navVisitEntry(element) {
    // const navigationExtras: NavigationExtras = {
    //   state: { propertyType: propertyType, propertyCode: propertyCode },
    // };
    this.router.navigate(['home/visitentry'], {
      state: { data: element },
    });
  }
  navMHCRegistration(element) {
    // const navigationExtras: NavigationExtras = {
    //   state: { propertyType: propertyType, propertyCode: propertyCode },
    // };
    this.router.navigate(['home/mhcregistration'], {
      state: { data: element },
    });
  }
}

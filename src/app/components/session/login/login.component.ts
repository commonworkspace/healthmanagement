import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'app/auth/auth.service';
import { AuthLoginInfo } from 'app/auth/login-info';
import { SessionStorageService } from 'app/auth/session-storage.service';
import { Md5 } from 'ts-md5/dist/md5';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'ms-login-session',
  templateUrl: './login-component.html',
  styleUrls: ['./login-component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent {
  loginForm: FormGroup;
  private loginInfo: AuthLoginInfo;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private sessionService: SessionStorageService,
    private injector: Injector,
    public snackBar: MatSnackBar
  ) {}
  ngOnInit() {
    this.loginForm = this.fb.group({
      userName: ['', Validators.required],
      passWord: ['', [Validators.required]],
    });
  }

  login(formData) {
    console.log(formData);
    const md5 = new Md5();
    const passwordMd5 = md5.appendStr(formData.value.passWord).end();
    console.log(passwordMd5, 'passwordMd5');
    this.loginInfo = new AuthLoginInfo(
      'hmsbalaji',
      formData.value.userName,
      passwordMd5,
      '',
      'Chrome'
    );
    const login = {
      _parameters: [
        {
          login: this.loginInfo,
        },
      ],
    };
    const result = JSON.stringify(login);
    console.log(result);
    // this.router.navigate(['/home']);
    this.authService.attemptAuth(result).subscribe(
      (data) => {
        console.log(data);
        if (data.result[0].result) {
          this.sessionService.saveToken(
            data.result[0].result.s,
            data.result[0].result.NICKNAME,
            data.result[0].result.MBRANCH,
            data.result[0].result.MCOMPANY
          );
          const routerService = this.injector.get(Router);
          routerService.navigateByUrl('/home', { skipLocationChange: false });
        }
        if (data.result[0].error) {
          this.snackBar.open(data.result[0].error.msg, 'x', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      },
      (err) => {
        console.log(err.message);
      }
    );
  }
}

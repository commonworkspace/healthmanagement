export class DocterName {
  constructor(
    public text: string,
    public id: string,
    public oldid: string,
    public ov: string
  ) {}
}
export class ServiceName {
  constructor(
    public text: string,
    public id: string,
    public oldid: string,
    public ov: string
  ) {}
}
export class OpBillingmodel {
  constructor(
    public sname: string,
    public service_name: ServiceName,
    public serviceid: string,
    public ispackage: string,
    public type_of_pack: string,
    public lis_ris_code: string,
    public servicegroup: string,
    public servicegroupid: string,
    public servicetype: string,
    public servicetypeid: string,
    public department: string,
    public departmentid: string,
    public doctorname: string,
    public isdoctorinvolved: string,
    public sdoctorname: DocterName,
    public doctorid: string,
    public checkqty_editable: string,
    public checkrate_editable: string,
    public quantity: string,
    public arate: string,
    public rate: string,
    public concessionctype: string,
    public concessionvalue: string,
    public amount: string,
    public concession: string,
    public payable_amount: string,
    public hidden_payamount: string,
    public corp_excluded: string,
    public corp_disallowamt: string,
    public g_remarks: string
  ) {}
}
export class Record {
  constructor(
    public rowno: string,
    public text: string,
    public columns: OpBillingmodel
  ) {}
}
// export interface OpBillingmodel {
//   consRate: string;
//   consType: string;
//   discount: string;
//   docterName: string;
//   netAmount: string;
//   rate: string;
//   remarks: string;
//   service: string;
//   sno: string;
// }
export interface DocterName {
  rowno: string;
  doctor_name: string;
  doctorname: string;
  docdept: string;
  dtype: string;
  doctor_viewid: string;
  branchid: string;
  companyid: string;
}
export interface Service {
  rowno: string;
  cm_serviceid: string;
  servicename: string;
  servicetype: string;
  aqty: string;
  qty: string;
  repeat: 'T';
  servicetypeid: string;
  servicegroup: string;
  servicegroupid: string;
  department: string;
  departmentid: string;
  doctorinv: string;
  pers: string;
  rate_editable: string;
  qty_editable: string;
  bloodbank_flg: string;
  lis_ris_code: string;
  type_of_pack: string;
  ispackage: string;
}
export class OpBillingServices {
  sno: number;
  service: Service;
  docterName: DocterName;
  rate: string;
  discount: string;
  consType: string;
  consRate: string;
  concession: string;
  netAmount: string;
  remarks: string;
}

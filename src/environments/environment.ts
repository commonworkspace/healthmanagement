// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
// http://210.18.134.138:82/axpertwebx6scripts/
export const environment = {
  production: false,
  getiviewApi:
    'http://210.18.134.138:82/axpertwebx6scripts/AsbIViewRest.dll/datasnap/rest/TASBIViewREST/getiview',
  saveUrlApi:
    'http://210.18.134.138:82/axpertwebx6scripts/ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata',
};
